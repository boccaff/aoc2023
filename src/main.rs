use aoc2023::days;
use rayon::iter::{ParallelBridge, ParallelIterator};
use std::time::Instant;

fn get_solver(n: isize) -> fn() -> (String, String) {
    match n {
        1 => days::day01::solve,
        2 => days::day02::solve,
        3 => days::day03::solve,
        4 => days::day04::solve,
        5 => days::day05::solve,
        6 => days::day06::solve,
        7 => days::day07::solve,
        8 => days::day08::solve,
        9 => days::day09::solve,
        10 => days::day10::solve,
        11 => days::day11::solve,
        12 => days::day12::solve,
        13 => days::day13::solve,
        14 => days::day14::solve,
        15 => days::day15::solve,
        16 => days::day16::solve,
        17 => days::day17::solve,
        18 => days::day18::solve,
        19 => days::day19::solve,
        20 => days::day20::solve,
        21 => days::day21::solve,
        22 => days::day22::solve,
        23 => days::day23::solve,
        24 => days::day24::solve,
        25 => days::day25::solve,
        i => panic!("Day {} not implemented.", i),
    }
}

fn main() {
    let total = Instant::now();
    let mut out = (1..26)
        .into_iter()
        .par_bridge()
        .map(|i| {
            let solver = get_solver(i);
            let start = Instant::now();
            let res = solver();
            if i == 25 {
                (i, format!("Day {}: {}, {:.2?}", i, res.0, start.elapsed()))
            } else {
                (
                    i,
                    format!("Day {}: {} {}, {:.2?}", i, res.0, res.1, start.elapsed()),
                )
            }
        })
        .collect::<Vec<_>>();
    out.sort();
    out.iter().for_each(|l| println!("{}", l.1));
    println!("Total {:.2?}", total.elapsed());
}
