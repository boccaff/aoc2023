const NUMS: [(&str, i32); 29] = [
    ("eleven", 11),
    ("twelve", 12),
    ("thirteen", 13),
    ("fourteen", 14),
    ("fifteen", 15),
    ("sixteen", 16),
    ("seventeen", 17),
    ("eighteen", 18),
    ("nineteen", 19),
    ("zero", 0),
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
    ("0", 0),
    ("1", 1),
    ("2", 2),
    ("3", 3),
    ("4", 4),
    ("5", 5),
    ("6", 6),
    ("7", 7),
    ("8", 8),
    ("9", 9),
];

fn part1(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            line.chars()
                .filter(|c| c.is_digit(10))
                .map(|c| c.to_digit(10).unwrap())
                .collect::<Vec<u32>>()
        })
        .map(|line| line.first().unwrap() * 10 + line.last().unwrap())
        .sum::<u32>()
}

// based on https://www.reddit.com/r/adventofcode/comments/1883ibu/2023_day_1_solutions/kbj030h/
fn match_str_or_digit(line: &str) -> Option<i32> {
    for (k, v) in NUMS.iter() {
        if line.starts_with(k) {
            return Some(*v);
        }
    }
    None
}

fn find_first(line: &str) -> Option<i32> {
    for i in 0..line.len() {
        if let Some(v) = match_str_or_digit(&line[i..]) {
            if v > 10 {
                return Some(v / 10);
            } else {
                return Some(v);
            }
        }
    }
    None
}

fn find_last(line: &str) -> Option<i32> {
    for i in (0..line.len()).rev() {
        if let Some(v) = match_str_or_digit(&line[i..]) {
            if v > 10 {
                return Some(v.rem_euclid(10));
            } else {
                return Some(v);
            }
        }
    }
    None
}

fn part2(input: &str) -> i32 {
    input
        .lines()
        .map(|line| find_first(line).unwrap() * 10 + find_last(line).unwrap())
        .sum::<i32>()
}

pub fn solve() -> (String, String) {
    let input = std::fs::read_to_string("input/01.txt").unwrap();
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = std::fs::read_to_string("testdata/01a.txt").unwrap();
        let p1 = part1(&input);
        assert_eq!(p1, 142);
    }
    #[test]
    fn test_part2() {
        let input = std::fs::read_to_string("testdata/01b.txt").unwrap();
        let p2 = part2(&input);
        assert_eq!(p2, 281);
    }
}
