type Output = Vec<(Vec<usize>, Vec<usize>)>;

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to find: {}", filename))
        .lines()
        .map(|line| {
            let (_, line) = line.split_once(": ").unwrap();
            let (bet, result) = line.split_once(" | ").unwrap();
            (
                bet.split_whitespace()
                    .map(|x| x.parse::<usize>().unwrap())
                    .collect::<Vec<_>>(),
                result
                    .split_whitespace()
                    .map(|x| x.parse::<usize>().unwrap())
                    .collect::<Vec<_>>(),
            )
        })
        .collect()
}

fn part1(input: &Output) -> isize {
    input
        .iter()
        .filter_map(|(bet, result)| {
            let n = bet.iter().filter(|x| result.contains(x)).count();
            if n > 0 {
                Some(2_i32.pow(n as u32 - 1))
            } else {
                None
            }
        })
        .sum::<i32>() as isize
}

fn part2(input: &Output) -> usize {
    let mut res = vec![1_usize; input.len()];
    input.iter().enumerate().for_each(|(i, (bet, result))| {
        let n = bet.iter().filter(|x| result.contains(x)).count();
        if n > 0 {
            for j in 1..=n {
                res[i + j] += res[i];
            }
        }
    });
    res.iter().sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/04.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/04.txt");
        assert_eq!(part1(&input), 13);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/04.txt");
        assert_eq!(part2(&input), 30);
    }
}
