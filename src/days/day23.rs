use pathfinding::prelude::Matrix;

type Output = (Vec<(usize, usize)>, Vec<(usize, usize, usize, bool)>);

fn neighbours(p: (usize, usize), map: &Matrix<u8>) -> Vec<((usize, usize), bool)> {
    [(0, 1), (1, 0), (0, -1), (-1, 0)]
        .into_iter()
        .filter_map(|step| {
            if let Some(np) = map.move_in_direction(p, step) {
                match map.get(np) {
                    Some(b'v') => Some((np, step == (1, 0))),
                    Some(b'^') => Some((np, step == (-1, 0))),
                    Some(b'>') => Some((np, step == (0, 1))),
                    Some(b'<') => Some((np, step == (0, -1))),
                    Some(b'.') => Some((np, true)),
                    Some(b'#') => None,
                    None => None,
                    Some(x) => panic!("found {} on map", *x as char),
                }
            } else {
                None
            }
        })
        .collect::<Vec<_>>()
}

pub fn parse_input(filename: &str) -> Output {
    let input = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename));
    let rows = input.lines().count();
    let cols = input.lines().last().unwrap().len();

    let map = Matrix::from_vec(
        rows,
        cols,
        input
            .lines()
            .flat_map(|line| line.as_bytes())
            .map(|x| *x)
            .collect::<Vec<_>>(),
    )
    .unwrap();

    let end = (map.rows - 1_usize, map.columns - 2_usize);
    let start = (0_usize, 1_usize);

    let mut from = vec![start];
    let mut vertices = vec![start];
    let mut nodes = Vec::new();
    let mut seen = Vec::new();

    while let Some(node) = from.pop() {
        let mut candidates = vec![(node, node, 0, true)];
        while let Some((pos, lastpos, cost, upslope)) = candidates.pop() {
            let nbs = neighbours(pos, &map);
            if (nbs.len() > 2 && pos != node) || (pos == end) {
                if !seen.contains(&pos) {
                    from.push(pos);
                    vertices.push(pos);
                    seen.push(pos);
                }
                nodes.push((node, pos, cost, upslope));
            } else {
                for (n, move_upslope) in nbs {
                    if n != lastpos {
                        candidates.push((n, pos, cost + 1, upslope && move_upslope));
                    }
                }
            }
        }
    }

    vertices.sort();

    let nodes = nodes
        .iter()
        .filter(|(f, t, _, dir)| f != t && *dir)
        .map(|(f, t, c, dir)| {
            (
                vertices.iter().position(|x| x == f).unwrap(),
                vertices.iter().position(|x| x == t).unwrap(),
                *c,
                *dir,
            )
        })
        .collect::<Vec<_>>();

    (vertices, nodes)
}

pub fn part1((vertices, nodes): &Output) -> usize {
    let adjlist = (0..vertices.len())
        .map(|i| {
            nodes
                .iter()
                .filter(|ai| ai.0 == i && ai.3)
                .map(|ai| (ai.1, ai.2))
                .collect::<Vec<(usize, usize)>>()
        })
        .collect::<Vec<_>>();

    // track current node, bitmask for visited nodes, and cost
    assert!(vertices.len() < 128);
    let mut candidates = vec![[0, 1, 0]];
    let mut curr_max = 0;

    while let Some([last_pos, visited, curr_cost]) = candidates.pop() {
        if last_pos == vertices.len() - 1_usize && curr_cost > curr_max {
            curr_max = curr_cost;
            continue;
        }
        for (node, cost) in &adjlist[last_pos] {
            if (visited >> node & 1) == 0 {
                let new_visited = visited + (1 << node);
                candidates.push([*node, new_visited, curr_cost + cost]);
            }
        }
    }

    curr_max
}

// tracking step and position does not lead to the right position
// tracking position and cost won't suffice to prevent the loop
pub fn part2((vertices, nodes): &Output) -> usize {
    let mut adjlist = (0..vertices.len())
        .map(|i| {
            nodes
                .iter()
                .filter(|ai| ai.0 == i)
                .map(|ai| (ai.1, ai.2))
                .collect::<Vec<(usize, usize)>>()
        })
        .collect::<Vec<_>>();

    adjlist
        .iter_mut()
        .zip((0..vertices.len()).map(|i| {
            nodes
                .iter()
                .filter(|ai| ai.1 == i)
                .map(|ai| (ai.0, ai.2))
                .collect::<Vec<(usize, usize)>>()
        }))
        .for_each(|(a1, a2)| a1.extend(a2));

    assert!(vertices.len() < 128);
    let mut candidates = vec![[0, 1, 0]];
    let mut curr_max = 0;

    while let Some([last_pos, visited, curr_cost]) = candidates.pop() {
        if last_pos == vertices.len() - 1_usize && curr_cost > curr_max {
            curr_max = curr_cost;
            continue;
        }
        for (node, cost) in &adjlist[last_pos] {
            if (visited >> node & 1) == 0 {
                let new_visited = visited + (1 << node);
                candidates.push([*node, new_visited, curr_cost + cost]);
            }
        }
    }
    curr_max
}

pub fn solve() -> (String, String) {
    let map = parse_input("input/23.txt");
    (part1(&map).to_string(), part2(&map).to_string())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let map = parse_input("testdata/23.txt");
        assert_eq!(part1(&map), 94);
    }

    #[test]
    fn test_part2() {
        let map = parse_input("testdata/23.txt");
        assert_eq!(part2(&map), 154);
    }
}
