use pathfinding::directed::dijkstra::dijkstra;
use pathfinding::matrix::{directions, Matrix};

type Output = ((usize, usize), (usize, usize), Matrix<usize>);

#[allow(unused)]
fn print_path(weights: &Matrix<usize>, path: &[(usize, usize)]) {
    for y in 0..weights.rows {
        for x in 0..weights.columns {
            if path.contains(&(x, y)) {
                print!(".")
            } else {
                print!("{}", weights.get((y, x)).unwrap());
            }
        }
        println!("");
    }
}

fn parse_input(filename: &str) -> Output {
    let input = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| {
            line.as_bytes()
                .iter()
                .map(|c| (c - b'0') as usize)
                .collect::<Vec<usize>>()
        })
        .collect::<Vec<_>>();
    let rows = input.len();
    let columns = input[0_usize].len();
    let weights = Matrix::from_vec(
        rows,
        columns,
        input
            .into_iter()
            .flatten()
            //.map(|&x| x)
            .collect::<Vec<usize>>(),
    )
    .unwrap();
    let start = (0_usize, 0_usize);
    let goal = (rows - 1_usize, columns - 1_usize);
    (start, goal, weights)
}

fn sucessors(
    (from, last_step): &((usize, usize), (isize, isize)),
    min_steps: usize,
    max_steps: usize,
    map: &Matrix<usize>,
) -> Vec<(((usize, usize), (isize, isize)), usize)> {
    directions::DIRECTIONS_4
        .iter()
        .filter(|(i, j)| (-i, -j) != *last_step && (*i, *j) != *last_step)
        .flat_map(|s| {
            let mut cost = 0;
            let mut pos = Some(*from);
            (0..max_steps).filter_map(move |i| {
                if let Some(curr_pos) = pos {
                    if let Some(next_pos) = map.move_in_direction(curr_pos, *s) {
                        cost += *map.get(next_pos).unwrap();
                        pos = Some(next_pos);
                        if i >= min_steps {
                            Some(((next_pos, *s), cost))
                        } else {
                            None
                        }
                    } else {
                        pos = None;
                        None
                    }
                } else {
                    None
                }
            })
        })
        .collect::<Vec<_>>()
}

fn part1((start, goal, map): &Output) -> usize {
    let (_, cost) = dijkstra(
        &(*start, (0_isize, 0_isize)),
        |p| sucessors(p, 0, 3, &map),
        |p| p.0 == *goal,
    )
    .unwrap();
    cost
}

fn part2((start, goal, map): &Output) -> usize {
    let (_, cost) = dijkstra(
        &(*start, (0_isize, 0_isize)),
        |p| sucessors(p, 3, 10, &map),
        |p| p.0 == *goal,
    )
    .unwrap();
    cost
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/17.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/17.txt");
        assert_eq!(part1(&input), 102);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/17.txt");
        assert_eq!(part2(&input), 94);
    }

    #[test]
    fn test_part2b() {
        let input = parse_input("testdata/17b.txt");
        assert_eq!(part2(&input), 71);
    }
}
