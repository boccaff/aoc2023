type Output = String;

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to find the file: {}", filename))
}

fn solve_run((t, d): (f64, f64)) -> isize {
    // run should beat the record, solving for distance + 1
    let disc = (t * t - 4.0 * (d + 1.0)).sqrt();
    let x1 = ((t + disc) / 2.0).floor() as isize;
    let x2 = ((t - disc) / 2.0).ceil() as isize;

    x1 - x2 + 1_isize
}

fn part1(input: &Output) -> isize {
    let (time, distance) = input.split_once("\n").unwrap();
    time.split_whitespace()
        .skip(1)
        .zip(distance.split_whitespace().skip(1))
        .map(|(t, d)| (t.parse::<f64>().unwrap(), d.parse::<f64>().unwrap()))
        .map(|x| solve_run(x))
        .fold(1, |acc, x| acc * x)
}

fn part2(input: &Output) -> isize {
    let (time, distance) = input.trim().split_once("\n").unwrap();
    let (_, time) = time.split_once(":").unwrap();
    let time = time.replace(" ", "").parse::<f64>().unwrap();

    let (_, distance) = distance.split_once(":").unwrap();
    let distance = distance.replace(" ", "").parse::<f64>().unwrap();

    solve_run((time, distance))
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/06.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/06.txt");
        assert_eq!(part1(&input), 288);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/06.txt");
        assert_eq!(part2(&input), 71503);
    }
}
