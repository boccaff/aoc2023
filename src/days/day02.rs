const KEY: [isize; 3] = [12, 13, 14];

fn parse_input(filename: &str) -> Vec<Vec<[isize; 3]>> {
    std::fs::read_to_string(filename)
        .unwrap()
        .lines()
        .map(|line| {
            let (_, draws) = line.split_once(": ").unwrap();
            let mut games = Vec::new();
            draws.split("; ").for_each(|set| {
                let mut newarr = [0; 3];
                set.split(", ").for_each(|dice| {
                    let (num, col) = dice.split_once(" ").unwrap();
                    let num = num.parse::<isize>().unwrap();
                    match col {
                        "red" => {
                            newarr[0_usize] = num;
                        }
                        "green" => {
                            newarr[1_usize] = num;
                        }
                        "blue" => {
                            newarr[2_usize] = num;
                        }
                        _ => panic!("New color found: {}", col),
                    }
                });
                games.push(newarr)
            });
            games
        })
        .collect()
}

fn part1(input: &[Vec<[isize; 3]>]) -> usize {
    input
        .iter()
        .enumerate()
        .filter(|(_, games)| {
            games
                .iter()
                .all(|arr| arr.iter().zip(KEY).all(|(a, k)| k - a >= 0))
        })
        .map(|(i, _)| i + 1)
        .sum()
}

fn part2(input: &[Vec<[isize; 3]>]) -> isize {
    input
        .iter()
        .map(|game| {
            let greens = game.iter().map(|arr| arr[0]).max().unwrap();
            let reds = game.iter().map(|arr| arr[1]).max().unwrap();
            let blues = game.iter().map(|arr| arr[2]).max().unwrap();
            greens * reds * blues
        })
        .sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/02.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/02.txt");
        let p1 = part1(&input);
        assert_eq!(p1, 8);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/02.txt");
        let p2 = part2(&input);
        assert_eq!(p2, 2286);
    }
}
