type Output = Vec<(isize, isize)>;

fn parse_input(filename: &str) -> Output {
    let mut y = 0_isize;
    let mut offset = 0_isize;
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .chars()
        .enumerate()
        .filter_map(|(x, c)| {
            if c == '\n' {
                y += 1;
                offset = (x + 1) as isize;
                None
            } else if c == '#' {
                Some((x as isize - offset, y))
            } else {
                None
            }
        })
        .collect::<Vec<_>>()
}

fn calc_expansion(input: &[(isize, isize)], expansion: isize) -> Vec<(isize, isize)> {
    let mut rows = input.iter().map(|(_, y)| *y).collect::<Vec<_>>();
    rows.sort();
    rows.dedup();
    let mut columns = input.iter().map(|(x, _)| *x).collect::<Vec<_>>();
    columns.sort();
    columns.dedup();

    input
        .iter()
        .map(|(x, y)| {
            (
                *x + (expansion - 1) * (*x - columns.iter().position(|i| i == x).unwrap() as isize),
                *y + (expansion - 1) * (*y - rows.iter().position(|i| i == y).unwrap() as isize),
            )
        })
        .collect::<Vec<_>>()
}

fn part1(input: &Output) -> isize {
    let arr = calc_expansion(input, 2).to_vec();
    let mut s = 0;
    for i in 0..arr.len() {
        for j in 0..i {
            if i != j {
                s += (arr[i].0 - arr[j].0).abs() + (arr[i].1 - arr[j].1).abs();
            }
        }
    }
    s
}

fn part2(input: &Output, expansion: isize) -> isize {
    let arr = calc_expansion(input, expansion).to_vec();
    let mut s = 0;
    for i in 0..arr.len() {
        for j in 0..i {
            if i != j {
                s += (arr[i].0 - arr[j].0).abs() + (arr[i].1 - arr[j].1).abs();
            }
        }
    }
    s
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/11.txt");
    (
        part1(&input).to_string(),
        part2(&input, 1000000).to_string(),
    )
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/11.txt");
        assert_eq!(part1(&input), 374);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/11.txt");
        assert_eq!(part2(&input, 10), 1030);
        assert_eq!(part2(&input, 100), 8410);
    }
}
