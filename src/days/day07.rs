type Hand = (u8, u32, isize, [u8; 13]);
type Output = String;

fn parse_hand1(line: &str) -> Hand {
    let (hand, bid) = line.split_once(" ").unwrap();
    let bid = bid.parse::<isize>().unwrap();
    let mut res = [0_u8; 13];
    let mut handnum: u32 = 0;
    hand.chars().rev().enumerate().for_each(|c| match c {
        (x, 'A') => {
            res[0_usize] += 1_u8;
            handnum += 14 * 16_u32.pow(x as u32)
        }
        (x, 'K') => {
            res[1_usize] += 1_u8;
            handnum += 13 * 16_u32.pow(x as u32)
        }
        (x, 'Q') => {
            res[2_usize] += 1_u8;
            handnum += 12 * 16_u32.pow(x as u32)
        }
        (x, 'J') => {
            res[3_usize] += 1_u8;
            handnum += 11 * 16_u32.pow(x as u32)
        }
        (x, 'T') => {
            res[4_usize] += 1_u8;
            handnum += 10 * 16_u32.pow(x as u32)
        }
        (x, '9') => {
            res[5_usize] += 1_u8;
            handnum += 9 * 16_u32.pow(x as u32)
        }
        (x, '8') => {
            res[6_usize] += 1_u8;
            handnum += 8 * 16_u32.pow(x as u32)
        }
        (x, '7') => {
            res[7_usize] += 1_u8;
            handnum += 7 * 16_u32.pow(x as u32)
        }
        (x, '6') => {
            res[8_usize] += 1_u8;
            handnum += 6 * 16_u32.pow(x as u32)
        }
        (x, '5') => {
            res[9_usize] += 1_u8;
            handnum += 5 * 16_u32.pow(x as u32)
        }
        (x, '4') => {
            res[10_usize] += 1_u8;
            handnum += 4 * 16_u32.pow(x as u32)
        }
        (x, '3') => {
            res[11_usize] += 1_u8;
            handnum += 3 * 16_u32.pow(x as u32)
        }
        (x, '2') => {
            res[12_usize] += 1_u8;
            handnum += 2 * 16_u32.pow(x as u32)
        }
        (_, x) => panic!("Invalid parse_hand char: {}", x),
    });
    (find_hand_type1(res), handnum, bid, res)
}

fn parse_hand2(line: &str) -> Hand {
    let (hand, bid) = line.split_once(" ").unwrap();
    let bid = bid.parse::<isize>().unwrap();
    let mut res = [0_u8; 13];
    let mut handnum: u32 = 0;
    hand.chars().rev().enumerate().for_each(|c| match c {
        (x, 'A') => {
            res[0_usize] += 1_u8;
            handnum += 14 * 16_u32.pow(x as u32)
        }
        (x, 'K') => {
            res[1_usize] += 1_u8;
            handnum += 13 * 16_u32.pow(x as u32)
        }
        (x, 'Q') => {
            res[2_usize] += 1_u8;
            handnum += 12 * 16_u32.pow(x as u32)
        }
        (x, 'T') => {
            res[3_usize] += 1_u8;
            handnum += 11 * 16_u32.pow(x as u32)
        }
        (x, '9') => {
            res[4_usize] += 1_u8;
            handnum += 10 * 16_u32.pow(x as u32)
        }
        (x, '8') => {
            res[5_usize] += 1_u8;
            handnum += 9 * 16_u32.pow(x as u32)
        }
        (x, '7') => {
            res[6_usize] += 1_u8;
            handnum += 8 * 16_u32.pow(x as u32)
        }
        (x, '6') => {
            res[7_usize] += 1_u8;
            handnum += 7 * 16_u32.pow(x as u32)
        }
        (x, '5') => {
            res[8_usize] += 1_u8;
            handnum += 6 * 16_u32.pow(x as u32)
        }
        (x, '4') => {
            res[9_usize] += 1_u8;
            handnum += 5 * 16_u32.pow(x as u32)
        }
        (x, '3') => {
            res[10_usize] += 1_u8;
            handnum += 4 * 16_u32.pow(x as u32)
        }
        (x, '2') => {
            res[11_usize] += 1_u8;
            handnum += 3 * 16_u32.pow(x as u32)
        }
        (x, 'J') => {
            res[12_usize] += 1_u8;
            handnum += 2 * 16_u32.pow(x as u32)
        }
        (_, x) => panic!("Invalid parse_hand char: {}", x),
    });
    (find_hand_type2(res), handnum, bid, res)
}

// QQQJA, 483, 5
// T55J5, 684, 4
// KK677, 28, 3
// KTJJT, 220, 2
// 32T3K, 765, 1

fn find_hand_type1(hand: [u8; 13]) -> u8 {
    let five = hand.iter().position(|x| *x == 5_u8);
    let four = hand.iter().position(|x| *x == 4_u8);
    let three = hand.iter().position(|x| *x == 3_u8);
    let firsttwo = hand.iter().position(|x| *x == 2_u8);
    let secondtwo = hand.iter().rposition(|x| *x == 2_u8);

    match (five, four, three, firsttwo, secondtwo) {
        (Some(_), _, _, _, _) => 6_u8,
        (_, Some(_), _, _, _) => 5_u8,
        (_, _, Some(_), Some(_), _) => 4_u8,
        (_, _, Some(_), _, _) => 3_u8,
        (_, _, _, Some(f), Some(s)) => {
            if f != s {
                2_u8
            } else {
                1_u8
            }
        }
        (_, _, _, _, _) => 0_u8,
    }
}

fn find_hand_type2(hand: [u8; 13]) -> u8 {
    let five = hand.iter().position(|x| *x == 5_u8);
    let four = hand.iter().position(|x| *x == 4_u8);
    let three = hand.iter().position(|x| *x == 3_u8);
    let firsttwo = hand.iter().position(|x| *x == 2_u8);
    let secondtwo = hand.iter().rposition(|x| *x == 2_u8);
    let jays = hand[12_usize];

    match (five, four, three, firsttwo, secondtwo, jays) {
        (Some(_), _, _, _, _, _) => 6_u8,
        (_, Some(_), _, _, _, 1) => 6_u8,
        (_, Some(_), _, _, _, 4) => 6_u8,
        (_, Some(_), _, _, _, 0) => 5_u8,
        (_, _, Some(_), Some(_), _, 3) => 6_u8,
        (_, _, Some(_), Some(_), _, 2) => 6_u8,
        (_, _, Some(_), Some(_), _, 0) => 4_u8,
        (_, _, Some(_), _, _, 3) => 5_u8,
        (_, _, Some(_), _, _, 1) => 5_u8,
        (_, _, Some(_), _, _, 0) => 3_u8,
        (_, _, _, Some(f), Some(s), 2) => {
            if f != s {
                5_u8
            } else {
                3_u8
            }
        }
        (_, _, _, Some(f), Some(s), 1) => {
            if f != s {
                4_u8
            } else {
                3_u8
            }
        }
        (_, _, _, Some(f), Some(s), 0) => {
            if f != s {
                2_u8
            } else {
                1_u8
            }
        }
        (_, _, _, _, _, 1) => 1_u8,
        (_, _, _, _, _, 0) => 0_u8,
        _ => panic!(
            "Failed to parse with: {:?}",
            (five, four, three, firsttwo, secondtwo, jays)
        ),
    }
}

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename).unwrap_or_else(|_| panic!("Failed to read: {}", filename))
}

fn part1(input: &Output) -> isize {
    let mut games = input
        .lines()
        .map(|line| parse_hand1(&line))
        .collect::<Vec<Hand>>();
    games.sort();
    games
        .iter()
        .enumerate()
        .map(|(rank, (_, _, bid, _))| (rank + 1) as isize * *bid)
        .sum()
}

fn part2(input: &Output) -> isize {
    let mut games = input
        .lines()
        .map(|line| parse_hand2(&line))
        .collect::<Vec<Hand>>();
    games.sort();
    games
        .iter()
        .enumerate()
        .map(|(rank, (_, _, bid, _))| (rank + 1) as isize * *bid)
        .sum()
}

// 249603798 too low
// 250158451 too low

pub fn solve() -> (String, String) {
    let input = parse_input("input/07.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/07.txt");
        assert_eq!(part1(&input), 6440);
        //assert_eq!(part1(&input), 0);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/07.txt");
        assert_eq!(part2(&input), 5905);
    }
}
