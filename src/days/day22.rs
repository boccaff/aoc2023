//! Sand Slabs
//! Find out how the slabs will settle, and later answer a couple questions
//! Part 1: How many can be safely removed without making other brick fall?
//! Part 2: Which brick being removed will cause more bricks to fall?

// use rayon::prelude::*;

use scan_fmt::scan_fmt;
type Block = [usize; 6];

fn parse_line(line: &str) -> [usize; 6] {
    let (x0, y0, z0, x1, y1, z1) = scan_fmt!(
        line,
        "{d},{d},{d}~{d},{d},{d}",
        usize,
        usize,
        usize,
        usize,
        usize,
        usize
    )
    .unwrap();
    [x0, y0, z0, x1, y1, z1]
}

fn block_to_z(block: &Block, z: usize) -> Block {
    let mut b = *block;
    b[5] = z + b[5] - b[2];
    b[2] = z;
    b
}

fn settle_blocks(blocks: &mut Vec<Block>, width: usize, depth: usize) -> usize {
    let mut floor_max = vec![0; width * depth];
    let mut i = 0;

    for b in blocks.iter_mut() {
        let mut curr_z = 0;
        for xi in b[0]..=b[3] {
            for yi in b[1]..=b[4] {
                curr_z = curr_z.max(floor_max[xi + yi * width]);
            }
        }

        if b[2] != curr_z {
            *b = block_to_z(b, curr_z);
            i += 1;
        }
        for xi in b[0]..=b[3] {
            for yi in b[1]..=b[4] {
                floor_max[xi + yi * width] = curr_z + 1_usize + b[5] - b[2];
            }
        }
    }
    i
}

type Output = (Vec<Block>, usize, usize);
pub fn parse_input(filename: &str) -> Output {
    let mut max_delta_z = 0;
    let mut max_x = 0;
    let mut max_y = 0;
    let mut max_z = 0;

    let mut blocks: Vec<Block> = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| {
            let block = parse_line(line);
            max_delta_z = max_delta_z.max(block[5] - block[2]);
            max_x = max_x.max(block[3]);
            max_y = max_y.max(block[4]);
            max_z = max_z.max(block[5]);
            block
        })
        .collect();

    blocks.sort_by_key(|b| b[2]);
    max_x += 1;
    max_y += 1;

    settle_blocks(&mut blocks, max_x, max_y);

    (blocks, max_x, max_y)
}

fn block_floor(block: &Block) -> usize {
    block[2].min(block[5])
}

fn block_ceil(block: &Block) -> usize {
    block[2].max(block[5])
}

fn xy_interception(b0: &Block, b1: &Block) -> bool {
    let [p0x, p0y, _, p1x, p1y, _] = b0;
    let [q0x, q0y, _, q1x, q1y, _] = b1;
    (p0x <= q1x && q0x <= p1x) && (p0y <= q1y && q0y <= p1y)
}

fn can_fall(candidate: &Block, input: &[Block]) -> bool {
    let mut can_fall = true;
    for block in input.iter() {
        if block != candidate {
            // if a block intercept at x,y and is below, the candidate can't fall
            if block_floor(candidate) == block_ceil(block) + 1_usize
                && xy_interception(candidate, block)
            {
                can_fall = false;
                break;
            }
        }
    }
    can_fall
}

pub fn part1((input, width, depth): &Output) -> usize {
    let blocks = input.clone();
    blocks
        .iter()
        .filter(|b| {
            let mut alt_blocks = blocks.clone();
            alt_blocks.remove(alt_blocks.iter().position(|a| a == *b).unwrap());
            let x = settle_blocks(&mut alt_blocks, *width, *depth);
            x == 0
        })
        .count()
}

pub fn part2((input, width, depth): &Output) -> usize {
    let blocks = input.clone();
    blocks
        .iter()
        .map(|b| {
            let mut alt_blocks = blocks.clone();
            alt_blocks.remove(alt_blocks.iter().position(|a| *a == *b).unwrap());
            let x = settle_blocks(&mut alt_blocks, *width, *depth);
            x
        })
        .sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/22.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/22.txt");
        assert_eq!(part1(&input), 5);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/22.txt");
        assert_eq!(part2(&input), 7);
    }
}
