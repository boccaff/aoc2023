type Output = Vec<Vec<isize>>;

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| {
            line.split_whitespace()
                .map(|x| x.parse::<isize>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

fn calc_diffs(x: &[isize]) -> Vec<isize> {
    x.iter()
        .skip(1)
        .zip(x.iter())
        .map(|(x1, x0)| x1 - x0)
        .collect::<Vec<_>>()
}

fn estimate(x: &[isize]) -> isize {
    let diffs = calc_diffs(x);
    if diffs.iter().all(|x| *x == 0) {
        return 0_isize;
    } else {
        return diffs.last().unwrap() + estimate(&diffs);
    }
}

fn estimate_first(x: &[isize]) -> isize {
    let diffs = calc_diffs(x);
    if diffs.iter().all(|x| *x == 0) {
        return 0_isize;
    } else {
        return diffs.first().unwrap() - estimate_first(&diffs);
    }
}

fn part1(input: &Output) -> isize {
    input
        .iter()
        .map(|seq| estimate(seq) + seq.last().unwrap())
        .sum()
}

fn part2(input: &Output) -> isize {
    input
        .iter()
        .map(|seq| seq.first().unwrap() - estimate_first(seq))
        .sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/09.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/09.txt");
        assert_eq!(part1(&input), 114);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/09.txt");
        assert_eq!(part2(&input), 2);
    }
}
