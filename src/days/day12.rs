use crate::unsigned_int_vec;
use rayon::prelude::*;
use std::collections::HashMap;

type Line = (Vec<u8>, Vec<usize>);
type Output = Vec<Line>;

fn parse_line(line: &str) -> Line {
    let (springs, groups) = line.split_once(" ").unwrap();
    let groups = unsigned_int_vec(groups)
        .iter()
        .map(|x| *x as usize)
        .collect::<Vec<usize>>();
    let springs = springs.as_bytes().iter().map(|x| *x).collect::<Vec<u8>>();
    (springs, groups)
}

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| parse_line(line))
        .collect()
}

#[allow(dead_code)]
fn print_blocks(springs: &[u8], groups: &[usize]) {
    println!(
        "{} | {:?} | {},{}",
        springs.iter().map(|&c| c as char).collect::<String>(),
        groups,
        springs.len(),
        groups.len()
    );
}

fn countarrang(
    springs: &[u8],
    groups: &[usize],
    si: usize,
    gi: usize,
    cache: &mut HashMap<(usize, usize), usize>,
) -> usize {
    let ret;
    if let Some(x) = cache.get(&(si, gi)) {
        return *x;
    } else {
        if si == springs.len() {
            if gi == groups.len() {
                ret = 1;
            } else {
                ret = 0;
            }
        } else if gi == groups.len() {
            // all blocks are matched
            if springs[si..].iter().all(|&s| s != b'#') {
                // and the springs left are not damaged
                ret = 1;
            } else {
                ret = 0;
            }
        } else {
            // since we still have blocks
            let blocks = groups[gi];
            if blocks > springs[si..].len() {
                ret = 0;
            } else if springs[si] == b'.' {
                ret = countarrang(springs, groups, si + 1, gi, cache);
            } else {
                let nonzero = springs[si..(si + blocks)]
                    .iter()
                    .filter(|&&s| s != b'.')
                    .count();
                if groups[gi..].iter().sum::<usize>() > springs[si..].len() {
                    ret = 0;
                } else if springs[si..].len() == blocks {
                    // springs have only blocks length
                    if nonzero == blocks {
                        ret = 1;
                    } else {
                        ret = 0;
                    }
                } else {
                    // implied that springs[si..].len() > blocks
                    if nonzero == blocks && springs[si + blocks] != b'#' {
                        if springs[si] == b'?' {
                            ret = countarrang(springs, groups, si + blocks + 1, gi + 1, cache)
                                + countarrang(springs, groups, si + 1, gi, cache);
                        } else {
                            ret = countarrang(springs, groups, si + blocks + 1, gi + 1, cache);
                        }
                    } else {
                        if springs[si] == b'?' {
                            ret = countarrang(springs, groups, si + 1, gi, cache);
                        } else {
                            ret = 0;
                        }
                    }
                }
            }
        }
        cache.insert((si, gi), ret);
        return ret;
    }
}

fn part1(input: &Output) -> usize {
    input
        .par_iter()
        .map(|(springs, groups)| {
            let mut cache: HashMap<(usize, usize), usize> = HashMap::new();
            countarrang(springs, groups, 0_usize, 0_usize, &mut cache)
        })
        .sum()
}

fn part2(input: &Output) -> usize {
    input
        .par_iter()
        .map(|(springs, groups)| {
            let mut new_springs = springs.to_vec();
            let mut new_groups = groups.to_vec();
            for _ in 0..4 {
                new_springs.push(b'?');
                new_springs.extend(springs.iter());
                new_groups.extend(groups.iter());
            }
            let mut cache: HashMap<(usize, usize), usize> = HashMap::new();
            countarrang(&new_springs, &new_groups, 0_usize, 0_usize, &mut cache)
        })
        .sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/12.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("testdata/12.txt");
        assert_eq!(part1(&input), 21);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/12.txt");
        assert_eq!(part2(&input), 525152);
    }
}
