type Output = Vec<Block>;
type Block = Vec<Vec<bool>>;

fn parse_block(block: &str) -> Block {
    block
        .lines()
        .map(|line| {
            line.as_bytes()
                .iter()
                .map(|&c| c == b'#')
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .split("\n\n")
        .map(|block| parse_block(block))
        .collect::<Vec<_>>()
}

fn block_row_sum(block: &Block) -> Vec<usize> {
    block
        .iter()
        .map(|line| {
            line.iter()
                .rev()
                .enumerate()
                .map(|(i, x)| if *x { 2_usize.pow(i as u32) } else { 0_usize })
                .sum::<usize>()
        })
        .collect::<Vec<_>>()
}

fn block_col_sum(block: &Block) -> Vec<usize> {
    let n_rows = block.len();
    let n_cols = block.first().unwrap().len();
    (0..n_cols)
        .map(|col| {
            (0..n_rows)
                .map(|row| {
                    if block[row][col] {
                        2_usize.pow((n_rows - row) as u32)
                    } else {
                        0
                    }
                })
                .sum()
        })
        .collect::<Vec<usize>>()
}

fn check_palindrome(arr: &[usize], skip: Option<&usize>) -> usize {
    let pairs = arr
        .iter()
        .zip(arr.iter().skip(1))
        .enumerate()
        .filter(|(_, (x0, x1))| x0 == x1)
        .filter(|(i, _)| {
            if let Some(&x) = skip {
                i + 1_usize != x
            } else {
                true
            }
        })
        .map(|(i, _)| i + 1_usize)
        .collect::<Vec<usize>>();

    if pairs.len() == 0 {
        return 0_usize;
    } else {
        for l in &pairs {
            let n_checks: usize = *(l.min(&(arr.len() - l)));
            if n_checks != 1 && (1..=n_checks).all(|i| arr[l - i] == arr[l + i - 1_usize]) {
                return *l;
            }
        }
        for l in pairs {
            if l == 1 {
                return 1_usize;
            } else if l == arr.len() - 1_usize {
                return arr.len() - 1_usize;
            }
        }
    }
    0_usize
}

fn summarize_block(
    block: &Block,
    skip_row: Option<&usize>,
    skip_col: Option<&usize>,
) -> (usize, usize) {
    let brs = block_row_sum(block);
    let bcs = block_col_sum(block);
    (
        check_palindrome(&brs, skip_row),
        check_palindrome(&bcs, skip_col),
    )
}

fn summarize_smudge_block(block: &Block) -> usize {
    let n_rows = block.len();
    let n_cols = block.first().unwrap().len();
    let (p1, p2) = summarize_block(block, None, None);

    for i in 0..n_rows {
        for j in 0..n_cols {
            let mut b = block.clone();
            b[i][j] = !b[i][j];
            let (np1, np2) = summarize_block(&b, Some(&p1), Some(&p2));

            if np1 != p1 && np1 != 0 {
                return 100 * np1;
            } else if np2 != p2 && np2 != 0 {
                return np2;
            } else {
            }
        }
    }
    print_block(block);
    unreachable!()
}

fn print_block(block: &Block) {
    block.iter().for_each(|line| {
        line.iter()
            .for_each(|c| if *c { print!("#") } else { print!(".") });
        println!("");
    });
}

fn part1(input: &Output) -> usize {
    input
        .iter()
        .map(|block| {
            let (p1, p2) = summarize_block(block, None, None);
            100 * p1 + p2
        })
        .sum()
}

// 30084 is too low
// another  one close to 34k is too low also
fn part2(input: &Output) -> usize {
    input
        .iter()
        .map(|block| summarize_smudge_block(block))
        .sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/13.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/13.txt");
        assert_eq!(part1(&input), 405);
    }

    #[test]
    fn test_block199() {
        let input = parse_input("testdata/13c.txt");
        assert_eq!(part1(&input), 700);
        let input = parse_input("testdata/13b.txt");
        assert_eq!(part1(&input), 700);
        assert_eq!(part2(&input), 100);
    }

    #[test]
    fn test_block23() {
        let input = parse_input("testdata/13e.txt");
        assert_eq!(part1(&input), 1200);
        let input = parse_input("testdata/13e.txt");
        assert_eq!(part1(&input), 1200);
        assert_eq!(part2(&input), 100);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/13.txt");
        assert_eq!(part2(&input), 400);
    }
}
