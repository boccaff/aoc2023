#[derive(Debug, Copy, Clone)]
struct Range {
    source: isize,
    dest: isize,
    range: isize,
}

#[derive(Debug, Clone)]
pub struct Map {
    maps: Vec<Range>,
}

impl Range {
    fn from_slice(v: &[isize]) -> Self {
        Self {
            source: v[1_usize],
            dest: v[0_usize],
            range: v[2_usize],
        }
    }

    fn dest_edges(self) -> (isize, isize) {
        (self.dest, self.dest + self.range - 1)
    }

    fn sources_edges(&self) -> (isize, isize) {
        (self.source, self.source + self.range - 1)
    }

    fn reverse_contains(self, x: &isize) -> bool {
        x >= &self.dest && x < &(self.dest + self.range)
    }

    fn reverse_get(self, x: &isize) -> isize {
        x - self.dest + self.source
    }

    #[allow(dead_code)]
    fn get_dest(self) -> isize {
        self.dest
    }

    #[allow(dead_code)]
    fn get_source(self) -> isize {
        self.source
    }

    fn get(self, x: &isize) -> isize {
        x - self.source + self.dest
    }

    fn contains(self, x: &isize) -> bool {
        x >= &self.source && x < &(self.source + self.range)
    }
}

impl Map {
    fn get(&self, x: &isize) -> isize {
        for m in &self.maps {
            if m.contains(x) {
                return m.get(x);
            }
        }
        *x
    }

    fn reverse_get(&self, x: &isize) -> isize {
        for m in &self.maps {
            if m.reverse_contains(x) {
                return m.reverse_get(x);
            }
        }
        *x
    }

    #[allow(dead_code)]
    fn get_source(&self) -> Vec<isize> {
        let mut x = self.maps.iter().map(|m| m.get_source()).collect::<Vec<_>>();
        x.push(0_isize);
        x.sort_by_key(|x| -x);
        x
    }

    #[allow(dead_code)]
    fn dest_edges(&self) -> Vec<isize> {
        let mut res = Vec::new();
        &self.maps.iter().for_each(|m| {
            let (a, b) = m.dest_edges();
            res.push(a);
            res.push(b);
        });
        res
    }

    fn sources_edges(&self) -> Vec<isize> {
        let mut res = Vec::new();
        &self.maps.iter().for_each(|m| {
            let (a, b) = m.sources_edges();
            res.push(a);
            res.push(b);
        });
        res
    }
}

type Output = (Vec<isize>, Vec<Map>);

fn parse_range(input: &str) -> Map {
    Map {
        maps: input
            .lines()
            .skip(1)
            .map(|line| {
                Range::from_slice(
                    &line
                        .split_whitespace()
                        .map(|x| x.parse::<isize>().unwrap())
                        .collect::<Vec<isize>>(),
                )
            })
            .collect::<Vec<_>>(),
    }
}

pub fn parse_input(filename: &str) -> Output {
    let input = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to load: {}", filename));
    let mut iter = input.split("\n\n");
    let seeds = iter.next().unwrap();
    let (_, seeds) = seeds.split_once(": ").unwrap();
    let seeds = seeds
        .split_whitespace()
        .map(|x| x.parse::<isize>().unwrap())
        .collect::<Vec<_>>();

    (seeds, iter.map(|x| parse_range(x)).collect::<Vec<_>>())
}

fn follow_almanac(seed: &isize, maps: &[Map]) -> isize {
    maps.iter().fold(*seed, |acc, fromto| fromto.get(&acc))
}

pub fn part1((seeds, maps): &Output) -> isize {
    seeds.iter().map(|s| follow_almanac(s, maps)).min().unwrap()
}

#[allow(dead_code)]
fn follow_back(location: &isize, maps: &[Map]) -> isize {
    maps.iter()
        .rev()
        .fold(*location, |acc, fromto| fromto.reverse_get(&acc))
}

pub fn part2((seeds, maps): &Output) -> isize {
    let mut ranges = Vec::with_capacity(seeds.len() * 2);
    seeds.chunks(2).for_each(|x| {
        ranges.push(x[0_usize]);
        ranges.push(x[0_usize] + x[1_usize] - 1);
    });

    let seeds = seeds
        .chunks(2)
        .map(|x| (x[0_usize], x[0_usize] + x[1_usize]))
        .collect::<Vec<(isize, isize)>>();

    for m in maps.iter() {
        let mut new_ranges = Vec::with_capacity(ranges.len() * 2);
        for de in m.dest_edges() {
            new_ranges.push(de);
        }
        for r in ranges {
            new_ranges.push(m.get(&r));
        }
        ranges = new_ranges;
    }
    ranges.sort();
    ranges.dedup();

    for l in ranges {
        let s = follow_back(&l, maps);
        if seeds.iter().any(|(start, end)| s >= *start && s < *end) {
            return l;
        }
    }
    unreachable!()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/05.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/05.txt");
        assert_eq!(part1(&input), 35);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/05.txt");
        assert_eq!(part2(&input), 46);
    }

    #[test]
    fn test_part2i() {
        let input = parse_input("input/05.txt");
        assert_eq!(part2(&input), 9622622);
    }
}
