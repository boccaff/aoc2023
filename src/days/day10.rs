use std::collections::HashMap;
use std::collections::HashSet;

type Output = ((isize, isize), HashMap<(isize, isize), Pipe>);

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
enum Pipe {
    L,
    I,
    J,
    F,
    D,
    S,
}

impl Pipe {
    fn from_char(c: &u8) -> Self {
        match *c {
            b'L' => Self::L,
            b'|' => Self::I,
            b'J' => Self::J,
            b'F' => Self::F,
            b'-' => Self::D,
            b'7' => Self::S,
            x => panic!("Ivalid char for pipe: {}", x as char),
        }
    }
    fn follow(self, s: &u8) -> u8 {
        if *s == self.entries()[0_usize] {
            self.exits()[1_usize]
        } else {
            self.exits()[0_usize]
        }
    }

    fn exits(self) -> [u8; 2] {
        match self {
            Self::L => [0_u8, 1_u8],
            Self::I => [0_u8, 2_u8],
            Self::J => [0_u8, 3_u8],
            Self::F => [1_u8, 2_u8],
            Self::D => [1_u8, 3_u8],
            Self::S => [2_u8, 3_u8],
        }
    }

    fn entries(self) -> [u8; 2] {
        match self {
            Self::L => [2_u8, 3_u8],
            Self::I => [2_u8, 0_u8],
            Self::J => [2_u8, 1_u8],
            Self::F => [3_u8, 0_u8],
            Self::D => [3_u8, 1_u8],
            Self::S => [0_u8, 1_u8],
        }
    }

    fn is_entry(self, x: &u8) -> bool {
        self.entries().contains(x)
    }

    fn is_exit(self, x: &u8) -> bool {
        self.exits().contains(x)
    }
}

fn step(s: &u8) -> (isize, isize) {
    match s {
        0 => (0, 1),
        1 => (1, 0),
        2 => (0, -1),
        3 => (-1, 0),
        x => panic!("Invalid step: {}", x),
    }
}

fn walk((x, y): &(isize, isize), (dx, dy): &(isize, isize)) -> (isize, isize) {
    (x + dx, y + dy)
}

fn parse_input(filename: &str) -> Output {
    let mut map = HashMap::new();
    let mut pos: Option<(isize, isize)> = None;
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .rev()
        .enumerate()
        .for_each(|(j, line)| {
            line.as_bytes().iter().enumerate().for_each(|(i, c)| {
                if c != &b'.' && c != &b'S' {
                    map.insert((i as isize, j as isize), Pipe::from_char(c));
                }
                if c == &b'S' {
                    pos = Some((i as isize, j as isize));
                }
            })
        });
    if let Some(p) = pos {
        let mut start_entries = Vec::new();
        for s in [0_u8, 1_u8, 2_u8, 3u8] {
            let d = step(&s);
            let candidate = walk(&p, &d);
            if let Some(neigh) = map.get(&candidate) {
                if neigh.is_entry(&s) {
                    start_entries.push(s);
                }
            };
        }
        for pipe in [Pipe::L, Pipe::I, Pipe::J, Pipe::F, Pipe::D, Pipe::S] {
            if start_entries.iter().all(|e| pipe.is_exit(e)) {
                map.insert(p, pipe);
            }
        }
        return (p, map);
    } else {
        panic!("No starting position found!")
    }
}

//fn moves(p: u8, (x, y): &(usize, usize)) -> Vec<(usize, usize)> {
//match p {
//b'I' => Vec::from([(*x, *y - 1), (*x, *y + 1)]),
//b'-' => Vec::from([(*x - 1, *y), (*x + 1, *y)]),
//b'L' => Vec::from([(*x + 1, *y), (*x, *y + 1)]),
//b'J' => Vec::from([(*x - 1, *y), (*x, *y + 1)]),
//b'7' => Vec::from([(*x - 1, *y), (*x, *y - 1)]),
//b'F' => Vec::from([(*x + 1, *y), (*x, *y - 1)]),
//c => panic!("Invalid char: {} on map.", c as char),
//}
//}

fn follow_path(
    start: &(isize, isize),
    map: &HashMap<(isize, isize), Pipe>,
) -> (isize, Vec<(isize, isize)>) {
    let [mut s0, mut s1] = map[start].exits();
    let mut path0 = Vec::new();
    let mut path1 = Vec::new();

    let mut l0 = walk(start, &step(&s0));
    let mut l1 = walk(start, &step(&s1));
    let mut p0 = map[&l0];
    let mut p1 = map[&l1];
    let mut i = 1 as isize;
    path0.push(*start);
    path0.push(l0);
    path1.push(l1);

    loop {
        s0 = p0.follow(&s0);
        s1 = p1.follow(&s1);
        l0 = walk(&l0, &step(&s0));
        l1 = walk(&l1, &step(&s1));
        p0 = map[&l0];
        p1 = map[&l1];
        i += 1;

        path0.push(l0);
        path1.push(l1);
        if l0 == l1 {
            break;
        }
    }

    path0.extend_from_slice(&path1);

    (i, path0)
}

fn part1((start, map): &Output) -> isize {
    follow_path(start, map).0
}

fn pipecount(pipe: &Pipe) -> isize {
    match pipe {
        &Pipe::I => 1,
        &Pipe::F => 1,
        &Pipe::S => 1,
        _ => 0,
    }
}

fn part2((start, map): &Output) -> isize {
    let (_, path) = follow_path(start, map);

    let (x_max, x_min) = path.iter().fold((isize::MIN, isize::MAX), |acc, (x, _)| {
        (acc.0.max(*x), acc.1.min(*x))
    });
    let (y_max, y_min) = path.iter().fold((isize::MIN, isize::MAX), |acc, (_, y)| {
        (acc.0.max(*y), acc.1.min(*y))
    });

    let mut res = 0;
    let mut lp = HashSet::new();

    for p in &path {
        lp.insert(p);
    }

    for j in y_min..=y_max {
        let mut crossings = 0;
        for i in x_min..=x_max {
            if lp.contains(&(i, j)) {
                crossings += pipecount(&map[&(i, j)]);
            } else {
                if crossings.rem_euclid(2) != 0 {
                    res += 1;
                }
            }
        }
    }

    res
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/10.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/10.txt");
        assert_eq!(part1(&input), 4);
        let input = parse_input("testdata/10b.txt");
        assert_eq!(part1(&input), 8);
    }

    #[test]
    fn test_part2_small() {
        let input = parse_input("testdata/10c.txt");
        assert_eq!(part2(&input), 4);
        let input = parse_input("testdata/10d.txt");
        assert_eq!(part2(&input), 4);
    }

    #[test]
    fn test_part2_big() {
        let input = parse_input("testdata/10e.txt");
        assert_eq!(part2(&input), 8);
        let input = parse_input("testdata/10f.txt");
        assert_eq!(part2(&input), 10);
    }
}
