use scan_fmt::scan_fmt;

type Line = (u8, isize, u8, isize);
type Output = Vec<Line>;

fn parse_line(line: &str) -> Line {
    match scan_fmt!(line, "{} {d} (#{5x}{x})", char, isize, [hex usize], [hex usize]) {
        Ok((dir, steps, m, d)) => {
            let d = if d == 0 {
                b'R'
            } else if d == 1 {
                b'D'
            } else if d == 2 {
                b'L'
            } else {
                b'U'
            };
            (dir as u8, steps, d, m as isize)
        }
        Err(e) => panic!("Invalid line to parse: {}, with error {}", line, e),
    }
}

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| parse_line(line))
        .collect::<Vec<_>>()
}

fn calc_polygon(input: &[(u8, isize)]) -> Vec<(isize, isize)> {
    let mut pos = (0, 0);
    let mut step = (0, 0);
    let mut res = Vec::new();
    res.push(pos);

    input.iter().for_each(|(direction, steps)| {
        match direction {
            b'R' => step = (1, 0),
            b'D' => step = (0, 1),
            b'L' => step = (-1, 0),
            b'U' => step = (0, -1),
            _ => {}
        }
        pos = (pos.0 + step.0 * steps, pos.1 + step.1 * steps);
        res.push(pos);
    });
    res
}

fn polygon_area(poly: &[(isize, isize)]) -> isize {
    poly.iter()
        .zip(poly.iter().skip(1))
        .map(|((x0, y0), (x1, y1))| (y0 + y1) * (x0 - x1))
        .sum::<isize>()
        / 2
}

fn polygon_length(arr: &[(u8, isize)]) -> isize {
    arr.into_iter().map(|(_, x)| x).sum::<isize>()
}

fn part1(input: &Output) -> isize {
    let input = input
        .iter()
        .map(|(d, m, _, _)| (*d, *m))
        .collect::<Vec<(u8, isize)>>();
    let poly = calc_polygon(&input);
    let poly_area = polygon_area(&poly);
    let poly_length = polygon_length(&input);
    poly_area + poly_length / 2 + 1_isize
}

fn part2(input: &Output) -> isize {
    let input = input
        .iter()
        .map(|(_, _, d, m)| (*d, *m))
        .collect::<Vec<(u8, isize)>>();
    let poly = calc_polygon(&input);
    let poly_area = polygon_area(&poly);
    let poly_length = polygon_length(&input);
    poly_area + poly_length / 2 + 1_isize
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/18.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/18.txt");
        assert_eq!(part1(&input), 62);
        //assert_eq!(part1(&input), 0);
    }

    #[test]
    fn test_part1b() {
        let input = parse_input("testdata/18b.txt");
        assert_eq!(part1(&input), 86);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/18.txt");
        assert_eq!(part2(&input), 952408144115);
    }
}
