use crate::str_to_usize;
use hashbrown::HashSet;
use petgraph::stable_graph::NodeIndex;
use petgraph::stable_graph::StableUnGraph;
use petgraph::visit::{Dfs, EdgeRef};
use priority_queue::PriorityQueue;
use rand::{thread_rng, Rng};
use rustc_hash::{FxHashSet, FxHasher};
use std::collections::HashMap;
use std::hash::BuildHasherDefault;

type Output = (HashMap<usize, String>, Vec<(usize, Vec<usize>)>);

pub fn parse_input(filename: &str) -> Output {
    let mut map = HashMap::new();
    let mut res = Vec::new();
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .for_each(|line| {
            let (from, dests) = line.split_once(": ").unwrap();
            let mut tmp = Vec::new();
            let nfrom = str_to_usize(from);

            map.insert(nfrom, from.to_string());

            for dest in dests.split_whitespace() {
                let ndest = str_to_usize(dest);
                map.insert(ndest, dest.to_string());
                tmp.push(ndest);
            }
            res.push((nfrom, tmp));
        });

    (map, res)
}

fn build_graph(cons: &[(usize, Vec<usize>)]) -> StableUnGraph<usize, usize> {
    let mut graph = StableUnGraph::<usize, usize>::default();
    let mut node_map = HashMap::new();

    let mut nodes = cons.iter().map(|(n, _)| *n).collect::<Vec<usize>>();
    nodes.extend(cons.iter().flat_map(|(_, x)| x.iter().map(|x| *x)));
    nodes.sort();
    nodes.dedup();

    nodes.iter().for_each(|n| {
        let gnode = graph.add_node(1_usize);
        node_map.insert(n, gnode);
    });

    cons.iter().for_each(|(from, dests)| {
        dests.iter().for_each(|to| {
            graph.add_edge(node_map[from], node_map[to], 1_usize);
        })
    });
    graph
}

#[allow(non_snake_case)]
fn minimum_cut_phase(graph: &StableUnGraph<usize, usize>) -> (NodeIndex, NodeIndex) {
    let mut A = FxHashSet::with_capacity_and_hasher(
        graph.node_count(),
        BuildHasherDefault::<FxHasher>::default(),
    );
    let mut node_ordering = Vec::with_capacity(graph.node_count());

    let ai = graph.node_indices().next().unwrap();

    let mut H = PriorityQueue::with_capacity_and_hasher(
        graph.node_count(),
        BuildHasherDefault::<FxHasher>::default(),
    );

    A.insert(ai);
    node_ordering.push(ai);
    graph.edges(ai).for_each(|eix| {
        H.push(eix.target(), *eix.weight());
    });

    while A.len() < graph.node_count() {
        if let Some((zi, _)) = H.pop() {
            A.insert(zi);
            node_ordering.push(zi);
            graph.edges(zi).for_each(|eix| {
                if !A.contains(&eix.target()) {
                    if let Some(wi) = H.get_priority(&eix.target()) {
                        H.push(eix.target(), wi + *eix.weight());
                    } else {
                        H.push(eix.target(), *eix.weight());
                    }
                }
            });
        }
    }

    (node_ordering.pop().unwrap(), node_ordering.pop().unwrap())
}

#[allow(non_snake_case)]
fn stoer_wagner(
    input: StableUnGraph<usize, usize>,
) -> (usize, FxHashSet<NodeIndex>, FxHashSet<NodeIndex>) {
    let mut graph = input.clone();
    let mut i = 0;
    let mut contractions = Vec::new();
    let mut best_cut_weight = usize::MAX;
    let mut best_cut_step = 0_usize;

    while graph.node_count() > 2 {
        let (t, s) = minimum_cut_phase(&graph);
        let w = graph.edges(t).map(|eix| *eix.weight()).sum();
        if w < best_cut_weight {
            best_cut_weight = w;
            best_cut_step = i;
        }

        contractions.push((s, t));

        let t_edges = graph
            .edges(t)
            .map(|x| (x.target(), *x.weight()))
            .collect::<Vec<_>>();

        for (r, w) in t_edges {
            if r != s {
                if let Some(ei) = graph.find_edge(s, r) {
                    let rsw = graph.edge_weight_mut(ei).unwrap();
                    *rsw += w;
                } else {
                    graph.add_edge(r, s, w);
                }
            }
        }

        graph.remove_node(t);
        i += 1;
    }

    let pgraph = StableUnGraph::<usize, ()>::from_edges(&contractions[..best_cut_step]);
    let mut dfs = Dfs::new(&pgraph, contractions[best_cut_step].1);
    let mut partition = FxHashSet::with_capacity_and_hasher(
        best_cut_step * 2,
        BuildHasherDefault::<FxHasher>::default(),
    );
    while let Some(n) = dfs.next(&pgraph) {
        partition.insert(n);
    }

    let other_partition: FxHashSet<_> = input
        .node_indices()
        .filter(|nix| !partition.contains(nix))
        .collect();

    (best_cut_weight, partition, other_partition)
}

#[allow(unused)]
fn contract_random(graph: StableUnGraph<usize, usize>) -> StableUnGraph<usize, usize> {
    let mut graph = graph.clone();

    let mut rng = thread_rng();
    let rand_e = rng.gen_range(0..graph.edge_count());

    let ei = graph.edge_indices().nth(rand_e).unwrap();

    let (en1, en2) = graph.edge_endpoints(ei).unwrap();

    let gen1 = graph
        .edges(en1)
        .map(|x| (x.source(), x.target(), *x.weight()))
        .collect::<Vec<_>>();

    let gen2 = graph
        .edges(en2)
        .map(|x| (x.source(), x.target(), *x.weight()))
        .collect::<Vec<_>>();

    let new_node =
        graph.add_node(*graph.node_weight(en1).unwrap() + *graph.node_weight(en2).unwrap());
    for (s, t, w) in gen1 {
        if s == en1 {
            graph.add_edge(new_node, t, w);
        }
    }

    for (s, t, w) in gen2 {
        if s == en2 {
            graph.add_edge(new_node, t, w);
        }
    }

    graph.remove_node(en1);
    graph.remove_node(en2);
    graph
}

#[allow(unused)]
fn karger(graph_input: StableUnGraph<usize, usize>, conf: f64) -> Vec<usize> {
    let n = graph_input.node_count();
    let mut c = graph_input.edge_count();
    let mut res = Vec::new();
    let iters = (n * (n - 1)) * ((1.0 / conf).ln().floor() as usize) / 2;

    for _ in 0..iters {
        let mut graph = graph_input.clone();
        loop {
            graph = contract_random(graph);
            if graph.node_count() == 2 {
                break;
            }
        }

        if graph.edge_count() < c {
            res = graph
                .edge_indices()
                .map(|ei| *graph.edge_weight(ei).unwrap())
                .collect::<Vec<usize>>();
            c = graph.edge_count();
        }
    }
    res
}

#[allow(unused)]
fn karger_stein(graph_input: StableUnGraph<usize, usize>, conf: f64) -> Vec<usize> {
    let n = graph_input.node_count();
    let mut c = graph_input.edge_count();
    let mut res = Vec::new();
    let iters = ((n as f64).ln().ceil() as usize) * ((1.0 / conf).ln().floor() as usize) / 2;
    let n_phase1 = ((n as f64) / 1.4142) as usize + 1;

    for _ in 0..iters {
        let mut graph = graph_input.clone();

        while graph.node_count() >= n_phase1 {
            graph = contract_random(graph);
        }

        loop {
            let graph1 = contract_random(graph.clone());
            let graph2 = contract_random(graph);

            if graph1.edge_count() < graph2.edge_count() {
                graph = graph1;
            } else {
                graph = graph2;
            }

            if graph.node_count() == 2 {
                break;
            }
        }

        if graph.edge_count() < c {
            res = graph
                .edge_indices()
                .map(|ei| *graph.edge_weight(ei).unwrap())
                .collect::<Vec<usize>>();
            c = graph.edge_count();
        }
    }
    res
}

pub fn part1((_, res): &Output) -> usize {
    let graph = build_graph(res);
    let (_, l1, l2) = stoer_wagner(graph);
    l1.len() * l2.len()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/25.txt");
    (part1(&input).to_string(), "".to_string())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn stoer_wagner_testb() {
        let (_, res) = parse_input("testdata/25b.txt");
        let graph = build_graph(&res);
        let (mincut, a, b) = stoer_wagner(graph);
        assert_eq!(mincut, 1);
        assert_eq!(a.len() * b.len(), 16)
    }

    #[test]
    fn stoer_wagner_test() {
        let g = StableUnGraph::<usize, usize>::from_edges(&[
            (0, 1, 2),
            (0, 4, 3),
            (1, 2, 3),
            (1, 4, 2),
            (1, 5, 2),
            (2, 3, 4),
            (2, 6, 2),
            (3, 6, 2),
            (3, 7, 2),
            (4, 5, 3),
            (5, 6, 1),
            (6, 7, 3),
        ]);
        assert_eq!((7.into(), 6.into()), minimum_cut_phase(&g));
        let (mincut, _, _) = stoer_wagner(g);
        assert_eq!(mincut, 4)
    }

    #[test]
    fn test_part1() {
        let input = parse_input("testdata/25.txt");
        assert_eq!(part1(&input), 54);
    }

    #[test]
    fn test_mcp1() {
        let g = StableUnGraph::<usize, usize>::from_edges(&[
            (0, 1, 2),
            (0, 2, 1),
            (1, 3, 2),
            (2, 3, 1),
            (3, 4, 2),
        ]);
        assert_eq!((2.into(), 4.into()), minimum_cut_phase(&g))
    }
}
