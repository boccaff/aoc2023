use std::collections::HashSet;

use crate::u8neighbors_tuple;

type Output = (
    Vec<(usize, usize, usize, usize)>,
    HashSet<(usize, usize)>,
    Vec<(usize, usize)>,
);

const SYMBOLS: [u8; 10] = [b'#', b'%', b'&', b'*', b'+', b'-', b'/', b'=', b'@', b'$'];

fn parse_input(filename: &str) -> Output {
    let mut res: Vec<(usize, usize, usize, usize)> = Vec::new();
    let mut symbols: HashSet<(usize, usize)> = HashSet::new();
    let mut gears: Vec<(usize, usize)> = Vec::new();

    let mut num: i32 = 0;
    let mut e: u32 = 0;
    let mut pos: (usize, usize) = (0, 0);
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to find: {}", filename))
        .lines()
        .enumerate()
        .for_each(|(j, line)| {
            line.as_bytes().iter().rev().enumerate().for_each(|(i, c)| {
                if (48..58).contains(c) {
                    num += (c - b'0') as i32 * 10_i32.pow(e);
                    pos = (i, j);
                    e += 1;
                } else if num != 0 {
                    res.push((
                        line.len() - pos.0 - 1_usize,
                        pos.1,
                        e as usize,
                        num as usize,
                    ));
                    num = 0;
                    e = 0;
                } else {
                    num = 0;
                    e = 0;
                }

                if c == &b'*' {
                    gears.push((line.len() - 1_usize - i, j));
                }

                if SYMBOLS.contains(c) {
                    symbols.insert((line.len() - 1_usize - i, j));
                }
            });

            if num != 0 {
                res.push((
                    line.len() - pos.0 - 1_usize,
                    pos.1,
                    e as usize,
                    num as usize,
                ));
            };
            num = 0;
            e = 0;
        });
    (res, symbols, gears)
}

fn part1((numbers, symbols, _): &Output) -> usize {
    let mut res = 0;
    for (i0, j0, width, n) in numbers {
        if (0..*width).any(|w| {
            u8neighbors_tuple((*i0 + w, *j0))
                .iter()
                .any(|n| symbols.contains(n))
        }) {
            res += n;
        }
    }
    res
}

// number -> (i, j, )
fn is_gear_neighbor(ni: usize, nj: usize, nw: usize, gi: usize, gj: usize) -> bool {
    gi >= if ni > 0 { ni - 1_usize } else { ni }
        && gi <= ni + nw
        && gj >= if nj > 0 { nj - 1_usize } else { nj }
        && gj <= nj + 1_usize
}

fn part2((numbers, _, gears): &Output) -> usize {
    gears
        .iter()
        .map(|(gi, gj)| {
            let neighs = numbers
                .iter()
                .filter(|(i, j, w, _)| is_gear_neighbor(*i, *j, *w, *gi, *gj))
                .map(|(_, _, _, n)| *n)
                .collect::<Vec<usize>>();

            if neighs.len() == 2_usize {
                neighs.first().unwrap() * neighs.last().unwrap()
            } else {
                0_usize
            }
        })
        .sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/03.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part1() {
        let input = parse_input("testdata/03.txt");
        assert_eq!(part1(&input), 4361);

        let input = parse_input("testdata/03a.txt");
        assert_eq!(part1(&input), 8);

        let input = parse_input("testdata/03b.txt");
        assert_eq!(part1(&input), 4);

        let input = parse_input("testdata/03c.txt");
        assert_eq!(part1(&input), 2);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/03.txt");
        assert_eq!(part2(&input), 467835);
    }
}
