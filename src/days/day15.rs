type Output = Vec<Vec<usize>>;

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .trim()
        .split(",")
        .map(|seq| {
            seq.as_bytes()
                .iter()
                .map(|c| *c as usize)
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

fn calc_hash(s: &[usize]) -> usize {
    s.iter()
        .fold(0, |acc, x| ((acc + x) * 17_usize).rem_euclid(256_usize))
}

fn part1(input: &Output) -> usize {
    input.iter().map(|seq| calc_hash(seq)).sum()
}

fn encode_step(step: &[usize]) -> (usize, usize, bool, usize) {
    let p: usize;
    let is_pop = *step.last().unwrap() == (b'-' as usize);

    if is_pop {
        p = step.len() - 1_usize;
    } else {
        p = step.len() - 2_usize;
    }

    let b = calc_hash(&step[..p]);
    let label = step[..p]
        .iter()
        .rev()
        .enumerate()
        .map(|(i, x)| x * 10_usize.pow(i as u32))
        .sum();
    let focal = if !is_pop {
        *step.last().unwrap() - (b'0' as usize)
    } else {
        0_usize
    };

    (b, label, !is_pop, focal)
}

fn part2(input: &Output) -> usize {
    let mut boxes: Vec<Vec<(usize, usize)>> = vec![Vec::new(); 256];
    input.iter().for_each(|step| {
        let (whichbox, label, is_insert, focal) = encode_step(step);

        if is_insert {
            if let Some(p) = boxes[whichbox]
                .iter()
                .position(|(boxlabel, _)| *boxlabel == label)
            {
                boxes[whichbox][p] = (label, focal);
            } else {
                boxes[whichbox].push((label, focal));
            }
        } else {
            if let Some(p) = boxes[whichbox]
                .iter()
                .position(|(boxlabel, _)| *boxlabel == label)
            {
                boxes[whichbox].remove(p);
            }
        }
    });
    boxes
        .iter()
        .enumerate()
        .map(|(i, onebox)| {
            onebox
                .iter()
                .enumerate()
                .map(|(j, (_, f))| (i + 1) * (j + 1) * f)
                .sum::<usize>()
        })
        .sum()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/15.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/15.txt");
        assert_eq!(part1(&input), 1320);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/15.txt");
        assert_eq!(part2(&input), 145);
    }
}
