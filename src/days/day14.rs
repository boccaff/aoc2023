type Output = Vec<Vec<u8>>;

pub fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| {
            line.as_bytes()
                .iter()
                .map(|c| match c {
                    b'#' => 2_u8,
                    b'O' => 1_u8,
                    b'.' => 0_u8,
                    x => {
                        panic!("Unexpected char while parsing map: {x}")
                    }
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>()
}

#[allow(dead_code)]
fn print_map(map: &[Vec<u8>]) {
    map.iter().for_each(|line| {
        line.iter().for_each(|x| match x {
            0 => print!("."),
            1 => print!("O"),
            2 => print!("#"),
            _ => panic!("Something else in your map!"),
        });
        print!("\n")
    });
    println!("");
}

fn tilt_row(v: &mut Vec<u8>) {
    let mut s = 0;

    while s < v.len() {
        let mut nr = s;
        let mut r = 0;
        for i in s..v.len() {
            if v[i] == 1 {
                r += 1;
                v[i] = 0;
            } else if v[i] == 2 {
                nr = i;
                break;
            }
            nr = i + 1;
        }
        for i in (nr - r)..nr {
            v[i] = 1;
        }
        s = nr + 1;
    }
}

fn tilt(m: &mut Vec<Vec<u8>>) {
    for i in 0..m.len() {
        tilt_row(&mut m[i]);
    }
}

fn rotate_grid_cw(m: &[Vec<u8>]) -> Vec<Vec<u8>> {
    let rows = m.len();
    let cols = m[0].len();
    let mut res = m.to_vec();
    for j in 0..cols {
        for i in 0..rows {
            res[j][rows - i - 1] = m[i][j]
        }
    }
    res
}

#[allow(dead_code)]
fn rotate_grid_ccw(m: &[Vec<u8>]) -> Vec<Vec<u8>> {
    let rows = m.len();
    let cols = m[0].len();
    let mut res = m.to_vec();
    for i in 0..rows {
        for j in 0..cols {
            res[rows - j - 1][i] = m[i][j]
        }
    }
    res
}

fn cycle(m: &[Vec<u8>]) -> Vec<Vec<u8>> {
    let mut m = m.to_vec();
    (0..4).for_each(|_| {
        tilt(&mut m);
        m = rotate_grid_cw(&m);
    });
    m
}

fn calc_load(map: &[Vec<u8>]) -> usize {
    map.iter()
        .map(|line| {
            line.iter()
                .enumerate()
                .map(|(i, x)| if *x == 1_u8 { i + 1_usize } else { 0_usize })
                .sum::<usize>()
        })
        .sum()
}

fn calc_cycle(map: &Vec<Vec<u8>>, n: usize) -> usize {
    let mut map = map.to_vec();
    for _ in 0..n {
        map = cycle(&mut map);
    }
    calc_load(&map)
}

fn find_recurrence(arr: &[usize]) -> Option<(usize, usize)> {
    for o in 0..(arr.len() / 2_usize) {
        for r in 1..((arr.len() - o) / 2_usize) {
            let tests = arr
                .iter()
                .skip(o)
                .zip(arr.iter().skip(o + r))
                .all(|(t0, tr)| tr == t0);
            if tests {
                return Some((o as usize, r as usize));
            }
        }
    }
    None
}

pub fn part1(input: &Output) -> usize {
    let mut m = input.to_vec();
    m = rotate_grid_cw(&m);
    tilt(&mut m);
    calc_load(&m)
}

pub fn part2(input: &Output) -> usize {
    let mut map = input.to_vec();
    let mut loads = Vec::with_capacity(222);
    map = rotate_grid_cw(&map);

    // with a better cycle detection code, this could be
    // reduced to 90 for my input.
    (0..222).for_each(|_| {
        map = cycle(&mut map);
        loads.push(calc_load(&map));
    });

    let (_, period) = find_recurrence(&loads).unwrap();
    let i = 1_000_000_000 % period;
    // TODO: no need to restart from zero since we already
    // have the board up to loads.len(). The else branch could
    // cycle (period + i - loads.len()).
    if (period + i) <= loads.len() {
        return loads[(period + i - 1) as usize];
    } else {
        return calc_cycle(input, period + i);
    }
}
// 94536 too low

pub fn solve() -> (String, String) {
    let input = parse_input("input/14.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/14.txt");
        assert_eq!(part1(&input), 136);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/14.txt");
        assert_eq!(part2(&input), 64);
    }
}
