use hashbrown::HashSet;

use num::traits::Euclid;

type Output = ((isize, isize), (isize, isize), HashSet<(isize, isize)>);

fn parse_input(filename: &str) -> Output {
    let mut map = HashSet::new();
    let mut s = None;
    let mut height = 0;
    let mut width = 0;

    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .enumerate()
        .for_each(|(i, line)| {
            height = i as isize;
            line.as_bytes().iter().enumerate().for_each(|(j, c)| {
                if *c == b'.' {
                    map.insert((i as isize, j as isize));
                }
                if *c == b'S' {
                    s = Some((i as isize, j as isize));
                    map.insert((i as isize, j as isize));
                }
                width = j as isize;
            })
        });

    (s.unwrap(), (width, height), map)
}

fn take_step(
    positions: &HashSet<(isize, isize)>,
    map: &HashSet<(isize, isize)>,
    size: isize,
    bound: bool,
) -> HashSet<(isize, isize)> {
    let mut new_positions = HashSet::new();
    for (ci, cj) in positions.iter() {
        [(0, 1), (0, -1), (1, 0), (-1, 0)]
            .iter()
            .map(|(di, dj)| (ci + di, cj + dj))
            .filter(|(ni, nj)| {
                if bound {
                    map.contains(&(*ni, *nj))
                } else {
                    map.contains(&(
                        ni.rem_euclid(&(size + 1_isize)),
                        nj.rem_euclid(&(size + 1_isize)),
                    ))
                }
            })
            .for_each(|n| {
                new_positions.insert(n);
            });
    }
    new_positions
}

fn step_count((start, size, map): &Output, max_steps: isize, bound: bool) -> usize {
    let mut candidates = HashSet::from([*start]);

    for _ in 0..max_steps {
        candidates = take_step(&candidates, map, size.0, bound);
    }
    candidates.len()
}

fn part1(input: &Output, max_steps: isize) -> usize {
    step_count(input, max_steps, true)
}

fn part2((start, size, map): &Output, w: usize) -> usize {
    let mut candidates = HashSet::from([*start]);

    for _ in 0..64 {
        candidates = take_step(&candidates, map, size.0, true);
    }
    let he = candidates.len();
    candidates = take_step(&candidates, map, size.0, true);
    let ho = candidates.len();
    for _ in 0..66 {
        candidates = take_step(&candidates, map, size.0, true);
    }
    let se = candidates.len();
    candidates = take_step(&candidates, map, size.0, true);
    let so = candidates.len();

    //    corners    + the ones without corner + tips           + full odds              + full evens
    // w * (se - he) + (w - 1) * (3 * so + ho) + 2 * so + 2 * ho + (w - 1) * (w - 1) * so + w * w * se - w
    w * (w + 1) * (se + so) + (w + 1) * ho - w * (he + 1)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/21.txt");
    (
        part1(&input, 64).to_string(),
        part2(&input, 202300).to_string(),
    )
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/21.txt");
        assert_eq!(part1(&input, 6), 16);
    }

    #[test]
    fn test_step_count() {
        let input = parse_input("testdata/21.txt");
        assert_eq!(step_count(&input, 10, false), 50);
    }

    #[test]
    #[ignore]
    fn slow_test_step_count() {
        let input = parse_input("testdata/21.txt");
        assert_eq!(step_count(&input, 50, false), 1594);
        assert_eq!(step_count(&input, 100, false), 6536);
        assert_eq!(step_count(&input, 500, false), 167004);
        //     assert_eq!(step_count(&input, 1000, false), 668697);
        //     assert_eq!(step_count(&input, 5000, false), 16733044);
    }

    #[test]
    #[ignore]
    fn test_part2() {
        let input = parse_input("input/21.txt");
        assert_eq!(part2(&input, 2), step_count(&input, 2 * 131 + 65, false));
        assert_eq!(part2(&input, 4), step_count(&input, 4 * 131 + 65, false));
        assert_eq!(part2(&input, 6), step_count(&input, 6 * 131 + 65, false));
    }
}
