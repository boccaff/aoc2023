use crate::signed_int_vec;
use itertools::Itertools;
use nalgebra::base::{ArrayStorage, Matrix2, SMatrix, Vector3};
use rayon::prelude::*;
type HailStone = (Vector3<f64>, Vector3<f64>);
type Output = Vec<HailStone>;

fn parse_input(filename: &str) -> Output {
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .map(|line| {
            let (pos, vel) = line.split_once(" @ ").unwrap();
            let pos = signed_int_vec(pos).into_iter().map(|x| x as f64).collect();
            let vel = signed_int_vec(vel).into_iter().map(|x| x as f64).collect();
            (Vector3::from_vec(pos), Vector3::from_vec(vel))
        })
        .collect::<Vec<_>>()
}

fn solve_p1((p1, v1): HailStone, (p2, v2): HailStone, (ll, ul): (f64, f64)) -> bool {
    let b = p2.xy() - p1.xy();
    let a = Matrix2::from_columns(&[v1.xy(), -v2.xy()]);
    let decomp = a.lu();
    if let Some(sol) = decomp.solve(&b) {
        let t1 = sol[0];
        let t2 = sol[1];
        if t1 > 0.0 && t2 > 0.0 {
            let s1 = p1 + v1 * t1;
            return s1[0] >= ll && s1[0] <= ul && s1[1] >= ll && s1[1] <= ul;
        } else {
            return false;
        }
    } else {
        false
    }
}

fn part1(input: &Output, limits: (f64, f64)) -> usize {
    input
        .iter()
        .tuple_combinations()
        .par_bridge()
        .filter(|(r1, r2)| solve_p1(**r1, **r2, limits))
        .count()
}

#[allow(non_snake_case)]
fn solve_p2(
    (pi, vi): &HailStone,
    (pj, vj): &HailStone,
    (pk, vk): &HailStone,
) -> Option<SMatrix<f64, 6, 1>> {
    let A: SMatrix<f64, 6, 6> = SMatrix::from_data(ArrayStorage([
        [vi.y - vj.y, vj.x - vi.x, 0.0, pj.y - pi.y, pi.x - pj.x, 0.0],
        [vi.z - vj.z, 0.0, vj.x - vi.x, pj.z - pi.z, 0.0, pi.x - pj.x],
        [0.0, vi.z - vj.z, vj.y - vi.y, 0.0, pj.z - pi.z, pi.y - pj.y],
        [vk.y - vj.y, vj.x - vk.x, 0.0, pj.y - pk.y, pk.x - pj.x, 0.0],
        [vk.z - vj.z, 0.0, vj.x - vk.x, pj.z - pk.z, 0.0, pk.x - pj.x],
        [0.0, vk.z - vj.z, vj.y - vk.y, 0.0, pj.z - pk.z, pk.y - pj.y],
    ]));
    let b: SMatrix<f64, 6, 1> = SMatrix::from_data(ArrayStorage([[
        pi.x * vi.y - pj.x * vj.y + pj.y * vj.x - pi.y * vi.x,
        pi.x * vi.z - pj.x * vj.z + pj.z * vj.x - pi.z * vi.x,
        pi.y * vi.z - pj.y * vj.z + pj.z * vj.y - pi.z * vi.y,
        pk.x * vk.y - pj.x * vj.y + pj.y * vj.x - pk.y * vk.x,
        pk.x * vk.z - pj.x * vj.z + pj.z * vj.x - pk.z * vk.x,
        pk.y * vk.z - pj.y * vj.z + pj.z * vj.y - pk.z * vk.y,
    ]]));

    // if let Ok(Ainv) = A.pseudo_inverse(1e-32) {
    //     Some(b.transpose() * Ainv)
    // } else {
    //     None
    // }
    let decomp = A.transpose().qr();
    decomp.solve(&b)
}

fn part2(input: &Output) -> isize {
    for v in (0..input.len()).combinations(3) {
        let si = input[v[0_usize]];
        let sj = input[v[1_usize]];
        let sk = input[v[2_usize]];

        if let Some(ans) = solve_p2(&si, &sj, &sk) {
            return (ans[0] + ans[1] + ans[2]).round() as isize;
        }
    }
    panic!()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/24.txt");
    (
        part1(&input, (200000000000000.0, 400000000000000.0)).to_string(),
        part2(&input).to_string(),
    )
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/24.txt");
        assert_eq!(part1(&input, (7.0, 27.0)), 2);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/24.txt");
        assert_eq!(part2(&input), 47);
    }
}
