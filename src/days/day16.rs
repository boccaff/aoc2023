use hashbrown::{HashMap, HashSet};
use rayon::prelude::*;

type Output = HashMap<(isize, isize), u8>;

fn parse_input(filename: &str) -> Output {
    let mut objects = HashMap::new();
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .enumerate()
        .for_each(|(j, line)| {
            line.as_bytes().iter().enumerate().for_each(|(i, c)| {
                if *c != b'.' {
                    objects.insert((i as isize, j as isize), *c);
                }
            })
        });
    objects
}

#[allow(dead_code)]
fn print_set(set: &HashSet<(isize, isize)>) {
    let (ui, li) = set.iter().fold((isize::MIN, isize::MAX), |acc, (x, _)| {
        (acc.0.max(*x), acc.1.min(*x))
    });

    let (uj, lj) = set.iter().fold((isize::MIN, isize::MAX), |acc, (_, y)| {
        (acc.0.max(*y), acc.1.min(*y))
    });

    for j in lj..=uj {
        for i in li..=ui {
            if set.contains(&(i, j)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        print!("\n")
    }
}

fn beam_step(
    (pi, pj): &(isize, isize),
    (di, dj): &(isize, isize),
    (i_max, i_min): &(isize, isize),
    (j_max, j_min): &(isize, isize),
) -> Option<(isize, isize)> {
    let qi = pi + di;
    let qj = pj + dj;

    if qi >= *i_min && qi <= *i_max && qj >= *j_min && qj <= *j_max {
        Some((qi, qj))
    } else {
        None
    }
}

fn contraption(input: &Output, pos: (isize, isize), step: (isize, isize)) -> usize {
    let mut beams = Vec::new();
    let mut energized = HashSet::new();
    let mut visited = HashSet::new();

    let i_range = input.keys().fold((isize::MIN, isize::MAX), |acc, (x, _)| {
        (acc.0.max(*x), acc.1.min(*x))
    });

    let j_range = input.keys().fold((isize::MIN, isize::MAX), |acc, (_, y)| {
        (acc.0.max(*y), acc.1.min(*y))
    });

    beams.push((pos, step));
    visited.insert((beam_step(&pos, &step, &i_range, &j_range).unwrap(), step));

    while let Some((mut pos, mut step)) = beams.pop() {
        'step_loop: while let Some(new_pos) = beam_step(&pos, &step, &i_range, &j_range) {
            energized.insert(new_pos);
            if let Some(obj) = input.get(&new_pos) {
                match obj {
                    b'/' => {
                        step = (-step.1, -step.0);
                    }
                    b'\\' => {
                        step = (step.1, step.0);
                    }
                    b'-' => {
                        if step.1 != 0 {
                            let new_step1 = (step.1, step.0);
                            let new_step2 = (-step.1, step.0);
                            if !visited.contains(&(new_pos, new_step1)) {
                                beams.push((new_pos, new_step1));
                                visited.insert((new_pos, new_step1));
                            }
                            if !visited.contains(&(new_pos, new_step2)) {
                                beams.push((new_pos, new_step2));
                                visited.insert((new_pos, new_step2));
                            }
                            break 'step_loop;
                        }
                    }
                    b'|' => {
                        if step.0 != 0 {
                            let new_step1 = (step.1, step.0);
                            let new_step2 = (step.1, -step.0);
                            if !visited.contains(&(new_pos, new_step1)) {
                                beams.push((new_pos, new_step1));
                                visited.insert((new_pos, new_step1));
                            }
                            if !visited.contains(&(new_pos, new_step2)) {
                                beams.push((new_pos, new_step2));
                                visited.insert((new_pos, new_step2));
                            }
                            break 'step_loop;
                        }
                    }
                    x => panic!("Unknown object found in the contraption: {}", *x as char),
                }
            }
            pos = new_pos;
        }
    }
    energized.len()
}

fn part1(input: &Output) -> usize {
    contraption(input, (-1, 0), (1, 0))
}

fn part2(input: &Output) -> usize {
    let (x_max, x_min) = input.keys().fold((isize::MIN, isize::MAX), |acc, (x, _)| {
        (acc.0.max(*x), acc.1.min(*x))
    });

    let (y_max, y_min) = input.keys().fold((isize::MIN, isize::MAX), |acc, (_, y)| {
        (acc.0.max(*y), acc.1.min(*y))
    });

    vec![
        (
            (1, 0),
            (y_min..=y_max)
                .map(|y| (-1, y))
                .collect::<Vec<(isize, isize)>>(),
        ),
        (
            (-1, 0),
            (y_min..=y_max)
                .map(|y| (x_max + 1, y))
                .collect::<Vec<(isize, isize)>>(),
        ),
        (
            (0, 1),
            (x_min..=x_max)
                .map(|x| (x, -1))
                .collect::<Vec<(isize, isize)>>(),
        ),
        (
            (0, -1),
            (x_min..=x_max)
                .map(|x| (x, y_max + 1))
                .collect::<Vec<(isize, isize)>>(),
        ),
    ]
    .into_iter()
    .map(|(step, pos_vector)| {
        pos_vector
            .iter()
            .par_bridge()
            .map(|pos| contraption(input, *pos, step))
            .max()
            .unwrap()
    })
    .max()
    .unwrap()
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/16.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/16.txt");
        assert_eq!(part1(&input), 46);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/16.txt");
        assert_eq!(part2(&input), 51);
    }
}
