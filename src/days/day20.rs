use crate::str_to_usize;
use std::collections::VecDeque;
type Output = (Vec<(Module, Vec<usize>)>, Vec<usize>);

#[derive(Debug, Clone)]
enum Module {
    Broadcast,
    Conjunction(Vec<Option<bool>>),
    FlipFlop(bool),
}

fn parse_input(filename: &str) -> Output {
    let mut res = Vec::new();
    let mut order = Vec::new();
    std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename))
        .lines()
        .for_each(|line| {
            let (module, destinations) = line.split_once(" -> ").unwrap();
            let destinations = destinations
                .trim()
                .split(", ")
                .map(|x| str_to_usize(x))
                .collect::<Vec<usize>>();
            match (&module[0_usize..1_usize], &module[1_usize..]) {
                ("%", name) => {
                    res.push((Module::FlipFlop(false), destinations));
                    order.push(str_to_usize(name));
                }
                ("&", name) => {
                    res.push((Module::Conjunction(Vec::new()), destinations));
                    order.push(str_to_usize(name));
                }
                ("b", _) => {
                    res.push((Module::Broadcast, destinations));
                    order.push(str_to_usize(module));
                }
                (_, _) => panic!("Cannot parse module: {module}"),
            }
        });

    for (_, dest) in res.iter_mut() {
        for e in dest.iter() {
            if !order.contains(e) {
                order.push(*e);
            }
        }
        for e in dest.iter_mut() {
            *e = order.iter().position(|i| *i == *e).unwrap();
        }
    }

    let mut tmp = Vec::new();
    for (i, (_, dest)) in res.iter().enumerate() {
        for d in dest {
            if d < &res.len() {
                match &res[*d].0 {
                    Module::Conjunction(_) => {
                        tmp.push((*d, i));
                    }
                    _ => {}
                }
            }
        }
    }

    for (to, from) in tmp {
        if let (Module::Conjunction(ref mut x), _) = res[to] {
            x.resize(from, None);
            x.insert(from, Some(false));
        }
    }

    (res, order)
}

fn propagate_signal(
    signal: bool,
    from: usize,
    module_idx: usize,
    system: &mut Vec<(Module, Vec<usize>)>,
) -> Vec<(usize, usize, bool)> {
    let dummy = system[module_idx].clone();
    let mut res = Vec::new();
    match &dummy {
        (Module::Broadcast, dest) => {
            for d in dest {
                res.push((module_idx, *d, false));
            }
        }
        (Module::Conjunction(_), dest) => {
            if let Module::Conjunction(ref mut srcs) = system[module_idx].0 {
                srcs[from] = Some(signal);
                if srcs.iter().filter_map(|x| *x).all(|x| x) {
                    for d in dest {
                        res.push((module_idx, *d, false));
                    }
                } else {
                    for d in dest {
                        res.push((module_idx, *d, true));
                    }
                }
            } else {
                panic!("This one here... you knew")
            }
        }
        (Module::FlipFlop(byte), dest) => {
            if !signal {
                if !byte {
                    for d in dest {
                        res.push((module_idx, *d, true));
                    }
                } else {
                    for d in dest {
                        res.push((module_idx, *d, false));
                    }
                }
                system[module_idx].0 = Module::FlipFlop(!byte);
            }
        }
    }
    res
}

fn part1((system, _): &Output) -> isize {
    let mut high = 0;
    let mut low = 0;

    let mut system = system.to_vec();

    let init = system
        .iter()
        .position(|(m, _)| match m {
            Module::Broadcast => true,
            _ => false,
        })
        .unwrap();
    for _ in 0..1_000 {
        let mut signals = VecDeque::new();
        signals.push_back((init, init, false));
        low += 1;

        while let Some((from, to, signal)) = signals.pop_front() {
            if to >= system.len() {
                continue;
            }
            let new_signals = propagate_signal(signal, from, to, &mut system);
            new_signals.iter().for_each(|message| {
                if message.2 {
                    high += 1;
                } else {
                    low += 1;
                }
                signals.push_back(*message);
            });
        }
    }
    high * low
}

#[allow(unused)]
fn find_recurrence(arr: &[isize]) -> Option<(isize, isize)> {
    for o in 0..(arr.len() / 2_usize) {
        for r in 1..((arr.len() - o) / 2_usize) {
            let tests = arr
                .iter()
                .skip(o)
                .zip(arr.iter().skip(o + r))
                .all(|(t0, tr)| tr == t0);
            if tests {
                return Some((o as isize, r as isize));
            }
        }
    }
    None
}

fn part2((system, nodes): &Output) -> usize {
    let mut system = system.to_vec();
    let rx_pos = nodes.iter().position(|x| *x == str_to_usize("rx")).unwrap();
    let n = 4100;
    let mut outer_counts: Vec<Vec<isize>> = vec![vec![0_isize; system.len() + 1_usize]; n];

    let init = system
        .iter()
        .position(|(m, _)| match m {
            Module::Broadcast => true,
            _ => false,
        })
        .unwrap();

    // inv map computes the sources inputs for each node
    // it will be used to check if all inputs of a node already have their
    // cycle detected
    // Assumption: there is a cycle for the inputs of nodes leading up to RX
    let mut inv_map = vec![Vec::new(); system.len() + 1_usize];
    system.iter().enumerate().for_each(|(i, (_, dest))| {
        dest.iter()
            // .filter(|d| d < &&system.len())
            .for_each(|d| inv_map[*d].push(i));
    });

    let rx_gp = inv_map[rx_pos]
        .iter()
        .flat_map(|i| inv_map[*i].clone())
        .collect::<Vec<_>>();

    for i in 0..n {
        let mut signals = VecDeque::new();
        signals.push_back((init, init, false));

        while let Some((from, to, signal)) = signals.pop_front() {
            if !signal {
                outer_counts[i][to] += 1;
            }
            if to >= system.len() {
                continue;
            }
            let new_signals = propagate_signal(signal, from, to, &mut system);
            new_signals.iter().for_each(|message| {
                signals.push_back(*message);
            });
        }
    }

    rx_gp
        .iter()
        .map(|i| outer_counts.iter().position(|v| v[*i] != 0).unwrap() + 1_usize)
        .fold(1_usize, |acc, x| acc * x)
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/20.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/20.txt");
        assert_eq!(part1(&input), 11687500);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("input/20.txt");
        assert!(part2(&input) > 1_000_000_000_000);
    }
}
