use crate::str_to_usize;
use scan_fmt::scan_fmt;
use std::collections::HashMap;

#[derive(Hash, Debug, Clone, Eq, PartialEq)]
struct Rule {
    attr: usize,
    greater: bool,
    threshold: usize,
    dest: Box<Rules>,
}

#[derive(Hash, Debug, Clone, Eq, PartialEq)]
struct EndRule {
    apprv: bool,
}

#[derive(Hash, Debug, Clone, Eq, PartialEq)]
enum Rules {
    Workflow(usize),
    Rule(Rule),
    EndRule(EndRule),
}

fn parse_rule(s: &str) -> Rules {
    //println!("in: {s}");
    if s == "A" {
        return Rules::EndRule(EndRule { apprv: true });
    } else if s == "R" {
        return Rules::EndRule(EndRule { apprv: false });
    } else if !s.contains(":") {
        Rules::Workflow(str_to_usize(s))
    } else {
        let (cattr, sign, threshold, dest) =
            scan_fmt!(s, "{[a-z]}{[<>]}{d}:{[a-zA-Z]}", char, char, usize, String).unwrap();
        let attr;
        match cattr {
            'x' => {
                attr = 0_usize;
            }
            'm' => {
                attr = 1_usize;
            }
            'a' => {
                attr = 2_usize;
            }
            's' => {
                attr = 3_usize;
            }
            x => panic!("Received rule for attribute: {}", x),
        }
        let dest = if dest == "A" {
            Rules::EndRule(EndRule { apprv: true })
        } else if dest == "R" {
            Rules::EndRule(EndRule { apprv: false })
        } else {
            Rules::Workflow(str_to_usize(&dest))
        };
        let greater = sign == '>';
        return Rules::Rule(Rule {
            attr,
            greater,
            threshold,
            dest: Box::new(dest),
        });
    }
}

fn parse_rule_line(line: &str) -> (usize, Vec<Rules>) {
    // opening curly brace position
    let ocb = line.chars().position(|c| c == '{').unwrap();
    // closing curly brace
    let ccb = line.len() - 1_usize;
    //println!(
    //"{line:?}, {:?}, {:?}",
    //&line[..ocb],
    //&line[(ocb + 1_usize)..ccb]
    //);
    let workflow = str_to_usize(&line[..ocb]);
    let rules = line[(ocb + 1_usize)..ccb]
        .split(",")
        .map(|r| {
            let x = parse_rule(r);
            //println!("{x:?}");
            x
        })
        .collect::<Vec<_>>();
    (workflow, rules)
}

fn parse_part(line: &str) -> [usize; 4] {
    let (x, m, a, s) = scan_fmt!(
        line,
        "{{x={d},m={d},a={d},s={d}}}",
        usize,
        usize,
        usize,
        usize
    )
    .unwrap();
    [x, m, a, s]
}

type Output = (HashMap<usize, Vec<Rules>>, Vec<[usize; 4]>);
fn parse_input(filename: &str) -> Output {
    let mut map = HashMap::new();
    let input = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename));
    let (rules, parts) = input.split_once("\n\n").unwrap();

    rules.lines().for_each(|line| {
        let (wf, rules) = parse_rule_line(line);
        map.insert(wf, rules);
    });
    let parts = parts
        .lines()
        .map(|line| parse_part(line))
        .collect::<Vec<_>>();

    (map, parts)
}

fn eval_rule(part: &[usize; 4], rules: &[Rules]) -> Rules {
    for rule in rules {
        match rule {
            Rules::Rule(rule) => {
                if rule.greater {
                    if part[rule.attr] > rule.threshold {
                        return *rule.dest.clone();
                    }
                } else {
                    if part[rule.attr] < rule.threshold {
                        return *rule.dest.clone();
                    }
                }
            }
            _ => return rule.clone(),
        }
    }
    unreachable!("Ruleset: {rules:?} did not terminate for part: {part:?}")
}

fn decide_part(part: &[usize; 4], system: &HashMap<usize, Vec<Rules>>, rule: &usize) -> bool {
    //println!("{rule:?}");
    let res = eval_rule(part, system.get(rule).unwrap());
    match res {
        Rules::EndRule(EndRule { apprv: ret }) => {
            return ret;
        }
        Rules::Workflow(x) => decide_part(part, system, &x),
        _ => panic!(
            "Failed to return endnoe for: {part:?} with rule {:?}",
            system.get(rule).unwrap()
        ),
    }
}

fn part1(input: &Output) -> usize {
    let (system, parts) = input;
    //system.iter().for_each(|x| println!("{x:?}"));
    //println!("{:?}", system.keys());
    parts
        .iter()
        .filter(|part| decide_part(part, system, &str_to_usize("in")))
        .map(|part| part.iter().sum::<usize>())
        .sum()
}

fn adjust_ranges(
    arr: &[(usize, usize); 4],
    pos: usize,
    greater: bool,
    threshold: usize,
) -> ([(usize, usize); 4], [(usize, usize); 4]) {
    let mut tarr = *arr;
    let mut farr = *arr;
    if greater {
        tarr[pos] = (tarr[pos].0.max(threshold), tarr[pos].1);
        farr[pos] = (farr[pos].0, farr[pos].1.min(threshold + 1));
    } else {
        tarr[pos] = (tarr[pos].0, tarr[pos].1.min(threshold));
        farr[pos] = (farr[pos].0.max(threshold - 1), farr[pos].1);
    }
    (tarr, farr)
}

fn calc_volume(arr: &[(usize, usize); 4]) -> usize {
    arr.iter().fold(
        1,
        |acc, x| {
            if x.1 > x.0 {
                acc * (x.1 - x.0 - 1)
            } else {
                0
            }
        },
    )
}

fn part2(input: &Output) -> usize {
    let (systems, _) = input;
    let mut candidates = vec![(
        str_to_usize("in"),
        [(0, 4001), (0, 4001), (0, 4001), (0, 4001)],
    )];
    let mut s = 0;

    while let Some((wf, mut ranges)) = candidates.pop() {
        let rules = systems.get(&wf).unwrap();
        for rule in rules {
            match rule {
                Rules::Rule(rule) => {
                    let (ranges_true, new_ranges) =
                        adjust_ranges(&ranges, rule.attr, rule.greater, rule.threshold);
                    ranges = new_ranges;
                    match *rule.dest {
                        Rules::Workflow(x) => {
                            candidates.push((x, ranges_true));
                        }
                        Rules::EndRule(EndRule { apprv: true }) => {
                            s += calc_volume(&ranges_true);
                        }
                        Rules::EndRule(EndRule { apprv: false }) => {}
                        _ => panic!("Rule: {rule:?} have a non-destination as destination!"),
                    }
                }
                Rules::Workflow(x) => {
                    candidates.push((*x, ranges));
                }
                Rules::EndRule(EndRule { apprv: true }) => {
                    s += calc_volume(&ranges);
                }
                Rules::EndRule(EndRule { apprv: false }) => {}
            }
        }
    }

    s
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/19.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/19.txt");
        assert_eq!(part1(&input), 19114);
    }
    #[test]
    fn test_part2() {
        let input = parse_input("testdata/19.txt");
        assert_eq!(part2(&input), 167409079868000);
    }
}
