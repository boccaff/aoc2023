use num::integer::lcm;
use std::collections::HashMap;

type Output = (Vec<bool>, HashMap<isize, [isize; 2]>);

const ABYTES: isize = b'A' as isize;
const ZOFFSET: isize = b'Z' as isize - ABYTES;

fn node_to_num(node: &str) -> isize {
    node.as_bytes()
        .iter()
        .rev()
        .enumerate()
        .map(|(i, c)| (*c as isize - ABYTES) * 100_isize.pow(i as u32))
        .sum()
}

fn parse_input(filename: &str) -> Output {
    let input = std::fs::read_to_string(filename)
        .unwrap_or_else(|_| panic!("Failed to read: {}", filename));
    let (inst, nodes) = input.split_once("\n\n").unwrap();
    let inst = inst.chars().map(|c| c == 'R').collect::<Vec<_>>();
    let mut map = HashMap::new();
    nodes.lines().for_each(|line| {
        let (from, to) = line.split_once(" = ").unwrap();
        let (left, right) = to[1_usize..(to.len() - 1_usize)].split_once(", ").unwrap();
        map.insert(node_to_num(from), [node_to_num(left), node_to_num(right)]);
    });
    (inst, map)
}

fn part1((inst, map): &Output) -> isize {
    let mut pos = node_to_num("AAA");
    let to = node_to_num("ZZZ");
    let mut i = 0_isize;
    let mut it = inst.iter().cycle();

    while pos != to {
        let turn_right = *it.next().unwrap();
        i += 1;
        if turn_right {
            pos = map[&pos][1_usize];
        } else {
            pos = map[&pos][0_usize];
        }
    }
    i
}

#[allow(dead_code)]
fn is_end(pos: &[isize]) -> bool {
    pos.iter().all(|x| x.rem_euclid(100) == ZOFFSET)
}

//fn part2((inst, map): &Output) -> isize {
//let mut pos = map
//.keys()
//.filter(|x| x.rem_euclid(100) == 0)
//.map(|x| *x)
//.collect::<Vec<isize>>();
//let mut i = 0_isize;
//let mut it = inst.iter().cycle();
//while !is_end(&pos) {
//let turn_right = *it.next().unwrap();
//i += 1;
//for j in 0..pos.len() {
//pos[j] = if turn_right {
//map[&pos[j]][1_usize]
//} else {
//map[&pos[j]][0_usize]
//};
//}
//}
//i
//}

fn part2((inst, map): &Output) -> isize {
    let pos = map
        .keys()
        .filter(|x| x.rem_euclid(100) == 0)
        .map(|x| *x)
        .collect::<Vec<isize>>();
    pos.iter()
        .map(|x| {
            let mut p = *x;
            let mut i = 0_isize;
            let mut it = inst.iter().cycle();

            while p.rem_euclid(100) != ZOFFSET {
                let turn_right = *it.next().unwrap();
                i += 1;
                if turn_right {
                    p = map[&p][1_usize];
                } else {
                    p = map[&p][0_usize];
                }
            }
            i
        })
        .fold(1_isize, |acc, x| lcm(acc, x))
}

pub fn solve() -> (String, String) {
    let input = parse_input("input/08.txt");
    (part1(&input).to_string(), part2(&input).to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_part1() {
        let input = parse_input("testdata/08.txt");
        assert_eq!(part1(&input), 6);
    }

    #[test]
    fn test_part2() {
        let input = parse_input("testdata/08.txt");
        assert_eq!(part2(&input), 6);
    }
}
