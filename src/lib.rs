//pub fn neighbors4_tuple((x, y): (isize, isize)) -> Vec<(isize, isize)> {
//[(0, 1), (1, 0), (-1, 0), (0, -1)]
//.iter()
//.map(|(dx, dy)| (x + dx, y + dy))
//.collect()
//}

//pub fn neighbors8_tuple((x, y): (isize, isize)) -> Vec<(isize, isize)> {
//[
//(0, 1),
//(1, 0),
//(-1, 0),
//(0, -1),
//(1, 1),
//(1, -1),
//(-1, 1),
//(-1, -1),
//]
//.iter()
//.map(|(dx, dy)| (x + dx, y + dy))
//.collect()
//}
//
pub mod days;
// TODO: Change it for consuming an iterator of a tuple type that implement order
// TODO: review day16
// Well well well, if it isn't the consequences of my own actions!
// TODO: refactor max_min_{x,y} to return (min, max) like a civilized
// range function should do.
pub fn max_min_x(slice: &[(isize, isize)]) -> (isize, isize) {
    slice.iter().fold((isize::MIN, isize::MAX), |acc, (x, _)| {
        (acc.0.max(*x), acc.1.min(*x))
    })
}

pub fn max_min_y(slice: &[(isize, isize)]) -> (isize, isize) {
    slice.iter().fold((isize::MIN, isize::MAX), |acc, (_, y)| {
        (acc.0.max(*y), acc.1.min(*y))
    })
}

// TODO: write tests for this
pub fn str_to_usize(s: &str) -> usize {
    s.as_bytes()
        .iter()
        .rev()
        .enumerate()
        .map(|(i, c)| (c - b'a') as usize * 26_usize.pow(i as u32))
        .sum()
}

pub fn signed_int_vec(s: &str) -> Vec<isize> {
    let mut v: Vec<isize> = Vec::with_capacity(s.len() / 2_usize);
    let mut i = 0_u32;
    let mut n = 0_isize;
    let mut is_neg = false;
    let mut is_number = false;
    s.bytes().rev().for_each(|c| {
        if c >= b'0' && c <= b'9' {
            is_number = true;
            n += (c - b'0') as isize * 10_isize.pow(i);
            i += 1;
        } else if c == b'-' {
            is_neg = true;
        } else if is_number {
            if is_neg {
                v.push(-n);
                is_neg = false;
                is_number = false;
            } else {
                v.push(n);
                is_neg = false;
                is_number = false;
            }
            n = 0;
            i = 0;
        } else {
            n = 0;
            i = 0;
        }
    });
    if n != 0 {
        if is_neg {
            v.push(-n);
        } else {
            v.push(n);
        }
    }
    v.iter().rev().map(|x| *x).collect()
}

pub fn unsigned_int_vec(s: &str) -> Vec<isize> {
    let mut v: Vec<isize> = Vec::with_capacity(s.len() / 2_usize);
    let mut i = 0_u32;
    let mut n = 0_isize;
    let mut is_number = false;
    s.bytes().rev().for_each(|c| {
        if c >= b'0' && c <= b'9' {
            is_number = true;
            n += (c - b'0') as isize * 10_isize.pow(i);
            i += 1;
        } else if is_number {
            v.push(n);
            is_number = false;
            n = 0;
            i = 0;
        } else {
            n = 0;
            i = 0;
        }
    });
    if n != 0 {
        v.push(n);
    }
    v.iter().rev().map(|x| *x).collect()
}

pub fn u8neighbors_tuple((x, y): (usize, usize)) -> Vec<(usize, usize)> {
    let mut res = Vec::from([
        (x + 1_usize, y),
        (x, y + 1_usize),
        (x + 1_usize, y + 1_usize),
    ]);

    //res.extend(Vec::from([
    //]))
    if x > 0 && y > 0 {
        res.push((x + 1_usize, y - 1_usize));
        res.push((x, y - 1_usize));
        res.push((x - 1_usize, y - 1_usize));
        res.push((x - 1_usize, y));
        res.push((x - 1_usize, y + 1_usize));
    } else if y > 0 {
        res.push((x + 1_usize, y - 1_usize));
        res.push((x, y - 1_usize));
    } else if x > 0 {
        res.push((x - 1_usize, y));
        res.push((x - 1_usize, y + 1_usize));
    }
    res
}

pub fn u4neighbors_tuple((x, y): (usize, usize)) -> Vec<(usize, usize)> {
    let mut res = Vec::from([(x + 1_usize, y), (x, y + 1_usize)]);

    //res.extend(Vec::from([
    //]))
    if x > 0 && y > 0 {
        res.push((x, y - 1_usize));
        res.push((x - 1_usize, y));
    } else if y > 0 {
        res.push((x, y - 1_usize));
    } else if x > 0 {
        res.push((x - 1_usize, y));
    }
    res
}

pub fn neighbors4((x, y): (isize, isize)) -> Vec<(isize, isize)> {
    [(0, 1), (1, 0), (0, -1), (-1, 0)]
        .iter()
        .map(|(dx, dy)| (x + dx, y + dy))
        .collect::<Vec<_>>()
}
// add a cycle detection algorithm
// parse list of ints without rellying on .parse()

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_unsigned_int_vec() {
        assert_eq!(
            unsigned_int_vec("10,   20, 30"),
            vec![10_isize, 20_isize, 30_isize]
        );
        assert_eq!(unsigned_int_vec("1 2 3"), vec![1_isize, 2_isize, 3_isize]);
        assert_eq!(
            unsigned_int_vec("15|24|33"),
            vec![15_isize, 24_isize, 33_isize]
        );
        assert_eq!(
            unsigned_int_vec("1,1,1,1,1,1,1"),
            vec![1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize]
        );
    }

    #[test]
    fn test_signed_int_vec() {
        assert_eq!(
            signed_int_vec("10,  -20,    30"),
            vec![10_isize, -20_isize, 30_isize]
        );
        assert_eq!(signed_int_vec("-1 2 3"), vec![-1_isize, 2_isize, 3_isize]);
        assert_eq!(
            signed_int_vec("-15|-24|-33"),
            vec![-15_isize, -24_isize, -33_isize]
        );
        assert_eq!(
            signed_int_vec("1,1,1,1,1,1,1"),
            vec![1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize, 1_isize]
        );
    }
}
