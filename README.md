# Goals

Run under 250 ms is the goal, less than 100 ms would be nice and less than 50 ms would be great. Current best result is [10 ms](https://www.reddit.com/r/adventofcode/comments/18wq48r/2023_rust_solving_entire_2023_in_10_ms/), with another nice reference for [25 ms](https://www.reddit.com/r/adventofcode/comments/190ano0/2023_all_daysrustpython_musings_on_rust_and/). The main idea is to proper benchmark each day, prioritize the larger runtimes and iterate. The whole year runs in 301.27s, including file reading, parsing and solution, in a very simple measurement:

```{shell}
% time ./target/release/aoc2023
./target/release/aoc2023  301.27s user 0.13s system 199% cpu 2:31.28 total
```

# Optimizing day 22

With the run time completely dominated by day 22, I did not see a point into implementing bechmarking or start profiling. The algorithm was bruteforcing in $n^3$ and that was probably compouding with the `HashSet`. After rewriting it to store the maximum $z$ over each $(x,y)$, a fighting a couple of bugs, run time dropped to close to 7 seconds for running everything (from file read to output).

```{shell}
% time ./target/release/aoc2023
./target/release/aoc2023  6.70s user 0.06s system 102% cpu 6.583 total
```

After that, implementing some basic measurement using `std::Instant`, thess are the results (with an associated 6.79s run):

```
Day 1: 54877 54100, 164.84µs
Day 2: 2810 69110, 80.75µs
Day 3: 553825 93994191, 994.25µs
Day 4: 26218 9997537, 165.08µs
Day 5: 510109797 9622622, 505.21ms
Day 6: 3317888 24655068, 23.92µs
Day 7: 248836197 251195607, 331.32µs
Day 8: 19783 9177460370549, 1.64ms
Day 9: 1842168671 903, 220.55µs
Day 10: 6733 435, 3.72ms
Day 11: 9805264 779032247216, 217.45µs
Day 12: 6949 51456609952403, 3.41ms
Day 13: 33520 34824, 4.73ms
Day 14: 109466 94585, 478.97ms
Day 15: 503154 251353, 311.98µs
Day 16: 8116 8383, 8.88ms
Day 17: 684 822, 107.47ms
Day 18: 46334 102000662718092, 351.87µs
Day 19: 495298 132186256794011, 903.37µs
Day 20: 898557000 238420328103151, 10.78ms
Day 21: 3605 596734624269210, 37.51ms
Day 22: 395 64714, 1.24s
Day 23: 2074 6494, 3.51s
Day 24: 25810 652666650475950, 4.47ms
Day 25: 583632, 871.83ms
```

That with some processing means:

| Day | Duration| Acc.Perc.|
|-----|---------|----------|
| 23  | 3.51s   |   49.65  |
| 22  | 1.24s   |   66.20  |
| 25  | 871.83ms|   80.63  |
| 5   | 505.21ms|   88.99  |
| 14  | 478.97ms|   96.91  |
| 17  | 107.47ms|   98.69  |
| 21  | 37.51ms |   99.31  |
| 20  | 10.78ms |   99.49  |
| 16  | 8.88ms  |   99.64  |
| 13  | 4.73ms  |   99.71  |
| 24  | 4.47ms  |   99.79  |
| 10  | 3.72ms  |   99.85  |
| 12  | 3.41ms  |   99.91  |
| 8   | 1.64ms  |   99.93  |
| 3   | 994.25µs|   99.95  |
| 19  | 903.37µs|   99.96  |
| 18  | 351.87µs|   99.97  |
| 15  | 311.98µs|   99.98  |
| 7   | 331.32µs|   99.98  |
| 9   | 220.55µs|   99.98  |
| 11  | 217.45µs|   99.99  |
| 4   | 165.08µs|   99.99  |
| 1   | 164.84µs|   99.99  |
| 2   | 80.75µs |   99.99  |
| 6   | 23.92µs |  100.00  |

Days 23, 22, 25, 5, and 14 are above 250 ms. And bringing them down to 250 and some parallel processing may be sufficient to reach the target. If all of the previous and also 17 are reduced below 100 ms, we have the secondary goal. And having all of them below 50 ms would be golden.

# First benchmarks and results

First it was needed to reorganize the code to make the functions accessible to the benchmarking code. That involved some refactor over calls to the internal libs. After implementing benchmarkings using the `std` tests and `criterion`, decided that the hassle of using two toolchains was higher than adding another package. Having `day23` as a clear target, used it to implement the benchmarks, and the results are:

``` 
parse_input             time:   [176.50 µs 176.91 µs 177.43 µs]
part1                   time:   [218.57 ms 218.86 ms 219.14 ms]
parse_map               time:   [43.706 ms 43.752 ms 43.799 ms]
part2                   time:   [3.4705 s 3.4754 s 3.4802 s]
``` 

This points directly at `part2`, that currently implement a DFS over `Vec`s being cloned when exploring neighbors. Also it uses `.contains(node)` and maybe that can be avoided. The algorithm along with cloning vecs are probably the main culprits, but this can be a nice thing to explore for profiling. After some setup, we find the runtime dominated by 

and, if I undestoodd correctly, a lot of idle time for the processors, that I guess are caused by waiting things to be allocated:

```
Performance counter stats for './target/release/aoc2023 count=10000':

          3,712.46 msec task-clock:u                     #    1.000 CPUs utilized
                 0      context-switches:u               #    0.000 /sec
                 0      cpu-migrations:u                 #    0.000 /sec
               559      page-faults:u                    #  150.574 /sec
    16,796,344,375      cycles:u                         #    4.524 GHz                         (83.33%)
        16,658,155      stalled-cycles-frontend:u        #    0.10% frontend cycles idle        (83.33%)
               799      stalled-cycles-backend:u         #    0.00% backend cycles idle         (83.33%)
    48,806,070,374      instructions:u                   #    2.91  insn per cycle
                                                  #    0.00  stalled cycles per insn     (83.33%)
    12,457,396,109      branches:u                       #    3.356 G/sec                       (83.35%)
       228,116,584      branch-misses:u                  #    1.83% of all branches             (83.34%)

       3.713096276 seconds time elapsed

       3.711856000 seconds user
       0.000999000 seconds sys
```

If we change the state enconding for a bitmask, which is possible since we have ~40 nodes after compressing the graph into the path bifurcations, we probably can avoid a lot of those. Changing this reduce the total runtime for less than 300 ms, and `part1` and `parse\_map` taking longer than part2.

Making `part1` compatible with part2, and improving on `parse_map` yields:
```
parse_input             time:   [1.0303 ms 1.0309 ms 1.0315 ms]
part1                   time:   [4.6393 µs 4.6509 µs 4.6634 µs]
part2                   time:   [135.96 ms 136.18 ms 136.41 ms]
```

# Optimizing day 22

The initial run for the benchmarks was quite a surprise, suggesting that I could just flip the part2 results to count when the number of parts settled would be 0 and be done with it. My previous impression was that I would need to implement the graph dependencies of blocks to allow a fast runtime.

```
parse_input             time:   [563.75 µs 564.82 µs 565.88 µs]
part1                   time:   [1.3605 s 1.3634 s 1.3666 s]
part2                   time:   [12.059 ms 12.072 ms 12.085 ms]
```

And there we have it: 

```
parse_input             time:   [564.72 µs 565.79 µs 566.95 µs]
part1                   time:   [11.884 ms 11.898 ms 11.913 ms]
part2                   time:   [12.010 ms 12.016 ms 12.023 ms]
```

Resulting in a total runtime of:

```
Day 25: 900.48ms
Day 14: 468.84ms
Day 5: 372.34ms
Day 23: 135.55ms
Day 17: 113.05ms
Day 21: 38.84ms
Day 22: 25.77ms
Day 20: 10.91ms
Day 16: 8.68ms
Day 13: 4.75ms
Day 24: 4.48ms
Day 10: 3.65ms
Day 12: 3.44ms
Day 8: 1.63ms
Day 3: 1.04ms
Day 19: 926.37µs
Day 7: 336.30µs
Day 18: 355.34µs
Day 15: 313.12µs
Day 9: 201.53µs
Day 11: 183.88µs
Day 1: 180.26µs
Day 4: 149.96µs
Day 2: 93.60µs
Day 6: 15.60µs
Total 2.10s
```

Probably some early exits in the settle block could be implemented, but there are slower code to look into here.

# Optimizing day 25

TODO: add runtimes for Karger and Karger-Stein, discussion over Girvan-Newman.

The first implementation of Stoer-Wagner had a runtime close to 250ms. After changing the `Hasher` for the `PriorityQueue` to `FxHasher`, it decreased to approximately 150ms. While this is a great improvement over the original 900 ms, and probably sufficient to run everything in 250 ms with a parallel run, it won't enable the secondary goals. Changing other hashsets brought a small 10 ms improvement, and I can't find other low hanging fruits for this implementation.

Other ideas to try is the Edmond-Karp algorithm, [Spectral bisection](https://www.reddit.com/r/adventofcode/comments/18qbsxs/2023_day_25_solutions/kgxsxbz/).
Additionally, I could benchmark the runtime of the solution using Stoer-Wagner from the `rustworkx_core` crate, Edmond-Karp from the `pathfinding` crate. If Stoer-Wagner/rustwork\_core is fast, maybe look into something in for my implementation, or have some idea of runtime before trying Edmond-Karp.

From the looks of the general result, it is probably better to tackle days 14 and 5, before coming back here.
```
Day 14: 109466 94585, 468.58ms
Day 5: 510109797 9622622, 361.15ms
Day 25: 583632, 145.77ms
Day 23: 2074 6494, 125.29ms
Day 17: 684 822, 98.74ms
Day 21: 3605 596734624269210, 37.41ms
Day 22: 395 64714, 25.51ms
Day 20: 898557000 238420328103151, 11.07ms
Day 16: 8116 8383, 9.22ms
Day 24: 25810 652666650475950, 4.65ms
Day 13: 33520 34824, 4.59ms
Day 10: 6733 435, 3.61ms
Day 12: 6949 51456609952403, 3.50ms
Day 8: 19783 9177460370549, 1.61ms
Day 3: 553825 93994191, 1.03ms
Day 19: 495298 132186256794011, 945.14µs
Day 18: 46334 102000662718092, 342.22µs
Day 7: 248836197 251195607, 327.11µs
Day 15: 503154 251353, 324.14µs
Day 9: 1842168671 903, 205.00µs
Day 11: 9805264 779032247216, 180.41µs
Day 4: 26218 9997537, 156.07µs
Day 2: 2810 69110, 86.49µs
Day 6: 3317888 24655068, 26.53µs
Day 1: 54877 54100, 160.67µs
Total 1.30s
```

Only 4 days exceed 100 ms (maybe 5, as day 17 maybe a close call), with ~7 exceeding 25 ms~ 5 exceeding 50 ms. There are currently ~10~ 18 days that in a sequential run could take less than ~25 ms~ 50 ms.

# Optimizing day 14

From benchmarking, parsing input took 16µs, and part1 took 560µs. Part2 take close to 500 ms, and since it does 4 * 222 evaluations analogous to part1, we are pretty close to just how long it should take, so the inner function need to be acelerated. Rewriting the algorithm to proccess all the rocks in a row:

```{rust}
fn tilt_row(v: &mut Vec<u8>) {
    let mut s = 0;

    while s < v.len() {
        let mut nr = s;
        let mut r = 0;
        for i in s..v.len() {
            if v[i] == 1 {
                r += 1;
                v[i] = 0;
            } else if v[i] == 2 {
                nr = i;
                break;
            }
            nr = i + 1;
        }
        for i in (nr - r)..nr {
            v[i] = 1;
        }
        s = nr + 1;
    }
}
```
While having a better recurrence function would probably help to cut runtime by half, `part1` new runtime decreased to 27µs, with `part2` runtime decreasing proportionally.
```
parse_input             time:   [16.873 µs 16.904 µs 16.933 µs]
part1                   time:   [27.107 µs 27.133 µs 27.158 µs]
part2                   time:   [31.410 ms 31.487 ms 31.619 ms]
```

With current runtime currently at:

```
Day 5: 510109797 9622622, 377.21ms
Day 25: 583632, 152.00ms
Day 23: 2074 6494, 131.73ms
Day 17: 684 822, 113.91ms
Day 21: 3605 596734624269210, 39.23ms
Day 14: 109466 94585, 31.38ms
Day 22: 395 64714, 25.62ms
Day 20: 898557000 238420328103151, 10.95ms
Day 16: 8116 8383, 9.10ms
Day 24: 25810 652666650475950, 4.79ms
Day 13: 33520 34824, 4.67ms
Day 10: 6733 435, 3.59ms
Day 12: 6949 51456609952403, 3.33ms
Day 8: 19783 9177460370549, 1.65ms
Day 3: 553825 93994191, 1.09ms
Day 1: 54877 54100, 167.48µs
Day 4: 26218 9997537, 155.96µs
Day 2: 2810 69110, 104.76µs
Day 19: 495298 132186256794011, 921.98µs
Day 18: 46334 102000662718092, 346.69µs
Day 7: 248836197 251195607, 334.61µs
Day 15: 503154 251353, 320.85µs
Day 9: 1842168671 903, 223.28µs
Day 11: 9805264 779032247216, 180.61µs
Day 6: 3317888 24655068, 25.14µs
Total 913.19ms
```

Finally broke the 1 s hurdle.

# Optimizing day 05

Initial runtime is 383ms for `part2` as expected, with 25µs for `parse_input`, 2µs for `part1`. Most of the time is spent in `follow_back`, which is called for every location from 0 untill it finds the location after 9.5 M attempts.

After the first rewrite to follow back from break points,
```
parse_input             time:   [23.975 µs 23.990 µs 24.007 µs]
part1                   time:   [1.2475 µs 1.2487 µs 1.2498 µs]
part2                   time:   [32.190 µs 32.213 µs 32.236 µs]
```

A second implementation used ranges edges as point of interest. This works due to the monotonicity of the functions, an for each step of mapping, the previous changepoints and the break points of the current map are added to a list of points of interest. After all theses points are mapped to a location and sorted, we follow back to a seed and if it matches a seed range, we return.

```
parse_input             time:   [24.019 µs 24.188 µs 24.398 µs]
part1                   time:   [1.5148 µs 1.5160 µs 1.5173 µs]
part2                   time:   [19.383 µs 19.414 µs 19.447 µs]
```

Everything runs under 100µs. 
 - 6 days take more than 25 ms
 - 3 days take more than 50 ms, and even 100 ms.
 - 18, 20 and 22 days have a combined runtime smaller than 25, 50 and 100 ms.

```
| Day | Time     |  Time [s] |  Acc.[s] | Acc.[%] |
|-----|----------|-----------|----------|---------|
| 25  | 156.42ms |    156.42 |  156.42  |  28.78% |
| 23  | 134.49ms |    134.49 |  290.91  |  53.53% |
| 17  | 114.85ms |    114.85 |  405.76  |  74.67% |
| 21  | 38.52ms  |     38.52 |  444.28  |  81.76% |
| 14  | 32.10ms  |     32.10 |  476.38  |  87.66% |
| 22  | 26.25ms  |     26.25 |  502.63  |  92.49% |
| 20  | 11.16ms  |     11.16 |  513.79  |  94.55% |
| 16  | 8.16ms   |      8.16 |  521.95  |  96.05% |
| 24  | 4.83ms   |      4.83 |  526.78  |  96.94% |
| 13  | 4.60ms   |      4.60 |  531.38  |  97.78% |
| 10  | 3.45ms   |      3.45 |  534.83  |  98.42% |
| 12  | 3.11ms   |      3.11 |  537.94  |  98.99% |
| 8   | 1.57ms   |      1.57 |  539.51  |  99.28% |
| 3   | 1.03ms   |      1.03 |  540.54  |  99.47% |
| 19  | 922.12µs |      0.92 |  541.46  |  99.64% |
| 18  | 369.67µs |      0.37 |  541.83  |  99.71% |
| 15  | 327.14µs |      0.33 |  542.16  |  99.77% |
| 7   | 317.08µs |      0.32 |  542.48  |  99.83% |
| 1   | 217.14µs |      0.22 |  542.69  |  99.87% |
| 11  | 205.68µs |      0.21 |  542.90  |  99.90% |
| 9   | 197.93µs |      0.20 |  543.10  |  99.94% |
| 4   | 148.07µs |      0.15 |  543.24  |  99.97% |
| 2   | 98.34µs  |      0.10 |  543.34  |  99.98% |
| 5   | 72.01µs  |      0.07 |  543.42  | 100.00% |
| 6   | 10.97µs  |      0.01 |  543.43  | 100.00% |
```

And with that, everything gets solved in ~ 180 ms.

# Sources

[The Rust Performance Book](https://nnethercote.github.io/perf-book/introduction.html)

# Related content

[Bertptrs, Advent of Code 2023: Let it snow](https://bertptrs.nl/2024/01/02/advent-of-code-2023-let-it-snow.html)


# Notes

## Keeping track:
    - Currently day 23 is a DFS without prunning. Some solutions using DP and/or graph properties can solve this in <30 ms, which is probably required for the 
    - Check back if `nalgebra` is using `lapack` or what. Since small matrices are used, this is probably not relevant.
 
## Goals for 2023:

Failed:
- Building up my lib

Suceeded:
- Using other libs:
    - rayon
    - itertools
    - scan\_fmt
    - petgraph
    - pathfinding
- Building a single program with days as modules.
- Measuring execution:
    - [X] Naively
    - [X] Criterion
- Profilling with perf
