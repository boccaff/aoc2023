use criterion::{criterion_group, criterion_main, Criterion};

pub fn day23(c: &mut Criterion) {
    // begin setup
    use aoc2023::days::day23::{parse_input, part1, part2};

    c.bench_function("parse_input", |b| {
        b.iter(|| {
            parse_input("./input/23.txt");
        })
    });

    let input = parse_input("./input/23.txt");
    c.bench_function("part1", |b| {
        b.iter(|| {
            part1(&input);
        })
    });

    let map = parse_input("./input/23.txt");
    c.bench_function("part2", |b| {
        b.iter(|| {
            part2(&map);
        })
    });
}

criterion_group!(benches, day23);
criterion_main!(benches);
